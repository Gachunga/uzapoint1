import { combineReducers } from 'redux';

import navReducer from '../reducers/nav';
import authReducer from '../reducers/auth';
import peopleReducer from '../reducers/people';
import loginReducer from '../reducers/login';
import signupReducer from '../reducers/signup';
import signupApiReducer from '../reducers/signupapi';
import posApiReducer from '../reducers/posapi';
import outletsReducer from '../reducers/outlets';
import customersReducer from '../reducers/customers';
import usersReducer from '../reducers/users';
import businessindustriesReducer from '../reducers/businessindustries';
import businesscategoriesReducer from '../reducers/businesscategories';
import productsuncategorizedReducer from '../reducers/pos/fetchproductsuncategorized';
import poscategoriesReducer from '../reducers/pos/productcategories';
import paymentmethodsReducer from '../reducers/pos/paymentmethods';
import posReducer from '../reducers/pos/pos';


export default combineReducers({
	auth: authReducer,
	nav: navReducer,
	people: peopleReducer,
	login: loginReducer,
	signup: signupReducer,
	businessindustries:businessindustriesReducer,
	businesscategories:businesscategoriesReducer,
	outlets:outletsReducer,
	users:usersReducer,
	signupapi: signupApiReducer,
	productsuncategorized: productsuncategorizedReducer,
	poscategories: poscategoriesReducer,
	paymentmethods: paymentmethodsReducer, 
	pos: posReducer,
	customers: customersReducer,
	posapi: posApiReducer   
});
