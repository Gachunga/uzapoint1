import { FETCHING_OUTLETS, FETCHING_OUTLETS_SUCCESS, FETCHING_OUTLETS_FAILURE} from './constants'

export function fetchOutletsFromAPI() {
  return (dispatch) => {
    dispatch(getOutlets())
    fetch('http://test.uzahost.com/adapter/client/business/no-of-stores')
    .then(data => data.json())
    .then(json => {
      console.log('json:', json)
      dispatch(getOutletsSuccess(json.data))
    })
    .catch(err => dispatch(getOutletsFailure(err)))
  }
}

export function getOutlets() {
  return {
    type: FETCHING_OUTLETS
  }
}

export function getOutletsSuccess(data) {
  return {
    type: FETCHING_OUTLETS_SUCCESS,
    data,
  }
}

export function getOutletsFailure() {
  return {
    type: FETCHING_OUTLETS_FAILURE
  }
}