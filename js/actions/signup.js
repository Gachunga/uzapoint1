import store from '../store';

import { ADD_BUSINESS_CATEGORIES, ADD_BUSINESS_INDUSTRIES, ADD_NUMBER_OF_OUTLETS, ADD_NUMBER_OF_USERS, ADD_BUSINESS_DETAILS, ADD_PERSONAL_DETAILS }
	from './constants';

const actionsCreator = {
	addBusinessIndustries: (payload) => {
		return {
			type: ADD_BUSINESS_INDUSTRIES,
			payload: payload,
		};
	},
	addBusinessCategories: (payload) => {
		return {
			type: ADD_BUSINESS_CATEGORIES,
			payload: payload,
		};
    },
	addNoOfOutlets: (payload) => {
		return {
			type: ADD_NUMBER_OF_OUTLETS,
			payload: payload,
		};
	},
	addNoOfUsers: (payload) => {
		return {
			type: ADD_NUMBER_OF_USERS,
			payload: payload,
		};
	},
	addBusinessDetails: (payload) => {
		return {
			type: ADD_BUSINESS_DETAILS,
			payload: payload,
		};
	},
	addPersonalDetails: (payload) => {
		return {
			type: ADD_PERSONAL_DETAILS,
			payload: payload,
		};
	},
};

const dispatchAction = function(action, payload){
	return store.dispatch(actionsCreator[action](payload));
};

export {
	dispatchAction,
	actionsCreator
};	

export default actionsCreator;
