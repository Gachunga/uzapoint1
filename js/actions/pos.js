import store from '../store';

import { ADD_CART_ITEM, ADD_CART_ITEMS, CLEAR_CART, REMOVE_ITEM, ADD_CUSTOMER, ADD_PAYMENT_DETAILS, SET_PRINT_STATUS, ADD_REFERENCE_NUMBER }
	from './constants';

const actionsCreator = {
	addCartItems: (payload) => {
		return {
			type: ADD_CART_ITEMS,
			payload: payload,
		};
	},
	addCartItem: (payload) => {
		return {
			type: ADD_CART_ITEM,
			payload: payload,
		};
    },
	addPaymentDetails: (payload) => {
		return {
			type: ADD_PAYMENT_DETAILS,
			payload: payload,
		};
	},
	clearCart: () => {
		return {
			type: CLEAR_CART,
		};
	},
	removeItem: (payload) => {
		return {
			type: REMOVE_ITEM,
			payload: payload,
		};
	},
	addCustomerDetail: (payload) => {
		return {
			type: ADD_CUSTOMER,
			payload: payload,
		};
	},
	addRefNo: (payload) => {
		return {
			type: ADD_REFERENCE_NUMBER,
			payload: payload,
		};
	},
};

const dispatchAction = function(action, payload){
	return store.dispatch(actionsCreator[action](payload));
};

export {
	dispatchAction,
	actionsCreator
};	

export default actionsCreator;
