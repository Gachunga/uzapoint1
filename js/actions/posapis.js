import { FETCHING_PRODUCT_LIST_UNCATEGORIZED, FETCHING_PRODUCT_LIST_UNCATEGORIZED_SUCCESS, FETCHING_PRODUCT_LIST_UNCATEGORIZED_FAILURE,
FETCHING_PRODUCT_CATEGORIES, FETCHING_PRODUCT_CATEGORIES_SUCCESS, FETCHING_PRODUCT_CATEGORIES_FAILURE, 
FETCHING_PRODUCT_SUB_CATEGORIES, FETCHING_PRODUCT_SUB_CATEGORIES_SUCCESS, FETCHING_PRODUCT_SUB_CATEGORIES_FAILURE,
FETCHING_PAYMENT_METHODS, FETCHING_PAYMENT_METHODS_SUCCESS, FETCHING_PAYMENT_METHODS_FAILURE } from './constants'

export function fetchuncategorizedproductlist(payload) {
    return (dispatch) => {
        dispatch(fetchProductsUncategorized())
        console.log(payload)
        fetch('http://heavenlyfood.uzahost.com/adapter/tgfc/product/list',{
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'userid': payload.info.id,
              'businesscode': payload.info.business_code,
              'token': payload.info.token,
            },
            })
            .then(data => data.json())
            .then(json => {
                console.log(json)
            if(json.status=='OK'){
              let payload=json.message
                dispatch(fetchProductsUncategorizedSuccess(payload))
            }else if(json.status=='ERROR'){
              let payload=json.message
              dispatch(fetchProductsUncategorizedFailure(payload))
            }
            
            })
            .catch(err => {
                console.log(err)
                dispatch(fetchProductsUncategorizedFailure('general error'))
            })
        }
}

export function fetchProductsUncategorized() {
  return {
    type: FETCHING_PRODUCT_LIST_UNCATEGORIZED
  }
}

export function fetchProductsUncategorizedSuccess(payload) {
  return {
    type: FETCHING_PRODUCT_LIST_UNCATEGORIZED_SUCCESS,
    payload,
  }
}
export function fetchProductsUncategorizedFailure(payload) {
  return {
    type: FETCHING_PRODUCT_LIST_UNCATEGORIZED_FAILURE,
    payload
  }
}

export function fetchproductcategories(payload) {
  return (dispatch) => {
      dispatch(fetchcategories())
      console.log(payload)
      fetch('http://heavenlyfood.uzahost.com/adapter/tgfc/product/categories',{
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'userid': payload.info.id,
            'businesscode': payload.info.business_code,
            'token': payload.info.token,
          },
          })
          .then(data => data.json())
          .then(json => {
              console.log(json)
          if(json.status=='OK'){
            let payload=json.data
              dispatch(fetchcategoriesSuccess(payload))
          }else if(json.status=='ERROR'){
            let payload=json.message
            dispatch(fetchcategoriesFailure(payload))
          }
          
          })
          .catch(err => {
              console.log(err)
              dispatch(fetchcategoriesFailure('general error'))
          })
      }
}

export function fetchcategories() {
return {
  type: FETCHING_PRODUCT_CATEGORIES
}
}

export function fetchcategoriesSuccess(payload) {
return {
  type: FETCHING_PRODUCT_CATEGORIES_SUCCESS,
  payload,
}
}
export function fetchcategoriesFailure(payload) {
return {
  type: FETCHING_PRODUCT_CATEGORIES_FAILURE,
  payload
}
}

export function fetchproductsubcategories(payload) {
  return (dispatch) => {
      dispatch(fetchsubcategories())
      console.log(payload)
      fetch('http://test.uzahost.com/adapter/tgfc/product/sub-categories/',{
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'user_id': payload.info.user.id,
            'business_code': payload.info.business_code,
            'token': payload.info.user.token,
          },
          })
          .then(data => data.json())
          .then(json => {
              console.log(json)
          if(json.status=='OK'){
            let payload=json.message
              dispatch(fetchsubcategoriesSuccess(payload))
          }else if(json.status=='ERROR'){
            let payload=json.message
            dispatch(fetchsubcategoriesFailure(payload))
          }
          
          })
          .catch(err => {
              console.log(err)
              dispatch(fetchsubcategoriesFailure('general error'))
          })
      }
}

export function fetchsubcategories() {
return {
  type: FETCHING_PRODUCT_CATEGORIES
}
}

export function fetchsubcategoriesSuccess(payload) {
return {
  type: FETCHING_PRODUCT_CATEGORIES_SUCCESS,
  payload,
}
}
export function fetchsubcategoriesFailure(payload) {
return {
  type: FETCHING_PRODUCT_CATEGORIES_FAILURE,
  payload
}
}


export function fetchpaymentmethods(payload) {
  return (dispatch) => {
      dispatch(fetchpmethods())
      console.log(payload)
      fetch('http://test.uzahost.com/adapter/tgfc/sale/payment-methods',{
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'userid': payload.info.id,
            'businesscode': payload.info.business_code,
            'token': payload.info.token,
          },
          })
          .then(data => data.json())
          .then(json => {
              console.log(json)
          if(json.status=='OK'){
            let payload=json.message
              dispatch(fetchpmethodsSuccess(payload))
          }else if(json.status=='ERROR'){
            let payload=json.message
            dispatch(fetchpmethodsFailure(payload))
          }
          
          })
          .catch(err => {
              console.log(err)
              dispatch(fetchpmethodsFailure('general error'))
          })
      }
}

export function fetchpmethods() {
return {
  type: FETCHING_PAYMENT_METHODS
}
}

export function fetchpmethodsSuccess(payload) {
return {
  type: FETCHING_PAYMENT_METHODS_SUCCESS,
  payload,
}
}
export function fetchpmethodsFailure(payload) {
return {
  type: FETCHING_PAYMENT_METHODS_FAILURE,
  payload
}
}