import { FETCHING_CATEGORIES, FETCHING_CATEGORIES_SUCCESS, FETCHING_CATEGORIES_FAILURE} from './constants'

export function fetchBusinessCategoriesFromAPI() {
  return (dispatch) => {
    dispatch(getBusinessCategories())
    fetch('http://test.uzahost.com/adapter/client/business/categories')
    .then(data => data.json())
    .then(json => {
      console.log('json:', json)
      dispatch(getBusinessCategoriesSuccess(json.data))
    })
    .catch(err => dispatch(getBusinessCategoriesFailure(err)))
  }
}

export function getBusinessCategories() {
  return {
    type: FETCHING_CATEGORIES
  }
}

export function getBusinessCategoriesSuccess(data) {
  return {
    type: FETCHING_CATEGORIES_SUCCESS,
    data,
  }
}

export function getBusinessCategoriesFailure() {
  return {
    type: FETCHING_CATEGORIES_FAILURE
  }
}