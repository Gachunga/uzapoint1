import { FETCHING_INDUSTRIES, FETCHING_INDUSTRIES_SUCCESS, FETCHING_INDUSTRIES_FAILURE} from './constants'

export function fetchBusinessIndustriesFromAPI() {
  return (dispatch) => {
    dispatch(getBusinessIndustries())
    fetch('http://test.uzahost.com/adapter/client/business/industries')
    .then(data => data.json())
    .then(json => {
      console.log('json:', json)
      dispatch(getBusinessIndustriesSuccess(json.data))
    })
    .catch(err => dispatch(getBusinessIndustriesFailure(err)))
  }
}

export function getBusinessIndustries() {
  return {
    type: FETCHING_INDUSTRIES
  }
}

export function getBusinessIndustriesSuccess(data) {
  return {
    type: FETCHING_INDUSTRIES_SUCCESS,
    data,
  }
}

export function getBusinessIndustriesFailure() {
  return {
    type: FETCHING_INDUSTRIES_FAILURE
  }
}