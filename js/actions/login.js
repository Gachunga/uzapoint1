import { AUTH_LOGGED, AUTH_LOGIN, LOGGING_IN, LOGIN_FAILURE, LOGIN_SUCCESS} from './constants'

export function loginFromAPI(user) {
  return (dispatch) => {
    dispatch(loginUser())
    console.log(user)
    fetch('http://heavenlyfood.uzahost.com/adapter/client/auth/login',{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: user.username,
            password: user.password
        }),
        })
        .then(data => data.json())
        .then(json => {
        if(json.status=='OK'){
          let user=json.data
        
          var payload={response:user, token: user.token, logged: true}
            dispatch(loginSuccess(user))
            dispatch(authSuccess(payload))
        }else if(json.status=='ERROR'){
          var payload=json.message
          dispatch(loginFailure(payload))
        }
        
        })
        .catch(err => {
            console.log(err)
        })
    }
}

export function loginUser() {
  return {
    type: LOGGING_IN
  }
}

export function loginSuccess(data) {
  return {
    type: LOGIN_SUCCESS,
    data,
  }
}
export function authSuccess(payload) {
  return {
    type: AUTH_LOGGED,
    payload,
  }
}
export function loginFailure(payload) {
  return {
    type: LOGIN_FAILURE,
    payload
  }
}

export function activateFromAPI(payload) {
  return (dispatch) => {
    dispatch(activate(payload))
    console.log(payload)
    fetch('http://test.uzahost.com/adapter/client/business/activate',{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'user_id': payload.info.user.id,
          'business_code': payload.info.business_code,
          'token': payload.info.user.token,
        },
        body: JSON.stringify({
            code: payload.code,
        }),
        })
        .then(data => data.json())
        .then(json => {
        if(json.status=='OK'){
          let payload=json.message
            dispatch(activateSuccess(payload))
        }else if(json.status=='ERROR'){
          var payload=json.message
          dispatch(activationFailure(payload))
        }
        
        })
        .catch(err => {
            console.log(err)
            dispatch(activationFailure('general error'))
        })
    }
}

export function activate() {
  return {
    type: 'ACTIVATING'
  }
}

export function activationSuccess(payload) {
  return {
    type: 'ACTIVATION_SUCCESS',
    payload,
  }
}
export function activationFailure(payload) {
  return {
    type: 'ACTIVATION_FAILURE',
    payload
  }
}