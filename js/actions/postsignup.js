import { SIGNNINGUP, SIGNUP_SUCCESS, SIGNUP_FAILURE } from './constants'

export function postSignUpToAPI(payload) {
  return (dispatch) => {
    dispatch(signupUser())
    dispatch(signingupUser())
    let  businessCategories = payload.businessCategories
    let  businessDetails = payload.businessDetails
    let  businessIndustry = payload.businessIndustry
    let  noOfOutlets = payload.noOfOutlets.noOfStores
    let  noOfUsers = payload.noOfUsers.noOfUsers
    let  personalDetails = payload.personalDetails

    let info={
      name: businessDetails.name,
      sub_domain: businessDetails.name,
      business_email: businessDetails.email,
      business_phone_number: businessDetails.phone,
      postal_address: businessDetails.postal_address,
      address: businessDetails.location,
      country: businessDetails.country,
      first_name: personalDetails.name,
      last_name: personalDetails.last_name,
      email: personalDetails.email,
      password: personalDetails.password,
      phone_number: personalDetails.phone,
      business_industry_code:businessIndustry.industry_code,
      business_category_code: [businessCategories.category_code],
      no_of_employees: noOfUsers,
      no_of_stores: noOfOutlets
  }
  console.log(info)
    fetch('http://test.uzahost.com/adapter/client/business/store',{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(info),
        })
        .then(data => data.json())
        .then(json => {
            console.log(json)
        if(json.status=='OK'){
          let payload=json.data
        
            dispatch(signupSuccess(payload))
            dispatch(restoreState())
        }else{
          var payload=json
          dispatch(signUpFailure(payload))
          dispatch(restoreState())
        }
        
        })
        .catch(err => {
            console.log(err)
            dispatch(signUpFailure(err))
            dispatch(restoreState())
        })
    }
}

export function signupUser() {
  return {
    type: SIGNNINGUP
  }
}
export function signingupUser() {
  return {
    type: 'SIGNNINGUPUSER'
  }
}
export function restoreState() {
  return {
    type: 'RESTORE_STATE'
  }
}
export function signupSuccess(payload) {
  return {
    type: SIGNUP_SUCCESS,
    payload,
  }
}
export function signUpFailure(payload) {
  return {
    type: SIGNUP_FAILURE,
    payload
  }
}