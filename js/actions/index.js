import store from '../store';

import { AUTH_LOGIN, AUTH_LOGGED, FETCHING_PEOPLE, FETCHING_PEOPLE_SUCCESS, FETCHING_PEOPLE_FAILURE }
	from './constants';

const actionsCreator = {
	authLogin: (payload) => {
		return {
			type: 'AUTH_LOGIN',
			payload: payload,
		};
	},
	authLogged: (payload) => {
		localStorage.authToken = payload.token || '';
		return {
			type: 'AUTH_LOGGED',
			payload: payload,
		};
	},
};

const dispatchAction = function(action, payload){
	return store.dispatch(actionsCreator[action](payload));
};

export {
	dispatchAction,
	actionsCreator
};

export default actionsCreator;
