import { FETCHING_CUSTOMERS, FETCHING_CUSTOMERS_SUCCESS, FETCHING_CATEGORIES_FAILURE, FETCHING_CUSTOMERS_FAILURE} from './constants'

export function fetchCustomersFromAPI(payload) {
    console.log(payload)
  return (dispatch) => {
    dispatch(getCustomers())
    fetch('http://heavenlyfood.uzahost.com/adapter/tgfc/customer/list',{
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'userid': payload.info.id,
          'businesscode': 'heav_KE',
          'token': payload.info.token,
        },
        })    .then(data => data.json())
    .then(json => {
      console.log('json:', json)
      if(json.status=='OK'){
        let payload=json.message
          dispatch(getCustomersSuccess(payload))
      }else if(json.status=='ERROR'){
        let payload=json.message
        dispatch(getCustomersFailure(payload))
      }
    })
    .catch(err => dispatch(getCustomersFailure(err)))
  }
}

export function getCustomers() {
  return {
    type: FETCHING_CUSTOMERS
  }
}

export function getCustomersSuccess(payload) {
  return {
    type: FETCHING_CUSTOMERS_SUCCESS,
    payload,
  }
}

export function getCustomersFailure() {
  return {
    type: FETCHING_CUSTOMERS_FAILURE
  }
}