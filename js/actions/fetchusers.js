import { FETCHING_USERS, FETCHING_USERS_SUCCESS, FETCHING_USERS_FAILURE} from './constants'

export function fetchUsersFromAPI() {
  return (dispatch) => {
    dispatch(getUsers())
    fetch('http://test.uzahost.com/adapter/client/business/no-of-users')
    .then(data => data.json())
    .then(json => {
      console.log('json:', json)
      dispatch(getUsersSuccess(json.data))
    })
    .catch(err => dispatch(getUsersFailure(err)))
  }
}

export function getUsers() {
  return {
    type: FETCHING_USERS
  }
}

export function getUsersSuccess(data) {
  return {
    type: FETCHING_USERS_SUCCESS,
    data,
  }
}

export function getUsersFailure() {
  return {
    type: FETCHING_USERS_FAILURE
  }
}