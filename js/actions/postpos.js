import { POSTING_POS, POSTING_POS_SUCCESS, POSTING_POS_FAILURE } from './constants'

export function postPosToAPI(payload) {
  return (dispatch) => {
    dispatch(postPos())
    let  posdetails = payload.data

    cartitems=posdetails.cartItems
    console.log(cartitems)
    product_codes=[]
    quantity=[]
    price=[]
    uom_code=[]
    total_amount=0
    total_balance=0
    cartitems.map((cartitem, i) => {
      if(product_codes.length==0){
        product_codes=[cartitem.product_code]
      }else{
        product_codes.push(cartitem.product_code)
      }
      if(quantity.length==0){
        quantity=[cartitem.cart_quantity]
      }else{
        quantity.push(cartitem.cart_quantity)
      }
      if(price.length==0){
        price=[cartitem.price]
      }else{
        price.push(cartitem.price)
      }
      if(uom_code.length==0){
        uom_code=[cartitem.uom_code]
      }else{
        uom_code.push(cartitem.uom_code)
      }
      
        total_amount=total_amount+parseFloat(cartitem.price)*parseInt(cartitem.cart_quantity)
      
    })

    let sale={
      product_code:product_codes,
      quantity:quantity,
      price:price,
      uom_code:uom_code,
      customer_id:posdetails.customerdetails.id,
      Longitude:'',
      Latitude:'',
      LocationName:'kiambu',
      printed:false,
      raise_order:false,
      location_code:'',
      storage_area_code:'',
      territory_code:'',
      paid_by:posdetails.paymentdetails,
      reference_name:posdetails.ref_no,
      reference_number:[],
      amount:[total_amount],
      reference_location:[],
      payment_note:'hello',
      total_amount: total_amount,
      total_balance: 0 
    }
  console.log(sale)
    fetch('http://test.uzahost.com/adapter/tgfc/sale/store',{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'userid': payload.info.id,
          'businesscode': payload.info.business_code,
          'token': payload.info.token,
        },
        body: JSON.stringify(sale),
        })
        .then(data => data.json())
        .then(json => {
            console.log(json)
        if(json.status=='OK'){
          let payload=json.data
        
            dispatch(postPosSuccess(payload))
            //dispatch(restoreState())
        }else{
          var payload=json
          dispatch(postPosFailure(payload))
          //dispatch(restoreState())
        }
        
        })
        .catch(err => {
            console.log(err)
            dispatch(postPosFailure(err))
            //dispatch(restoreState())
        })
    }
}

export function postPos() {
  return {
    type: POSTING_POS
  }
}
export function restoreState() {
  return {
    type: 'RESTORE_POS_STATE'
  }
}
export function postPosSuccess(payload) {
  return {
    type: POSTING_POS_SUCCESS,
    payload,
  }
}
export function postPosFailure(payload) {
  return {
    type: POSTING_POS_FAILURE,
    payload
  }
}