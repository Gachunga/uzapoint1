import { FETCHING_CUSTOMERS, FETCHING_CUSTOMERS_SUCCESS, FETCHING_CATEGORIES_FAILURE, FETCHING_CUSTOMERS_FAILURE} from '../actions/constants'
const initialState = {
  customers: [],
  isFetchingCustomers: false,
  success: false,
  error: false,
  errormessage: ''
}

export default function customers (state = initialState, action) {
  switch (action.type) {
    case FETCHING_CUSTOMERS:
      return {
        ...state,
        customers: [],
        isFetchingCustomers: true
      }
    case FETCHING_CUSTOMERS_SUCCESS:
      return {
        ...state,
        isFetchingCustomers: false,
        customers: action.payload,
        success: true
      }
    case FETCHING_CUSTOMERS_FAILURE:
      return {
        ...state,
        isFetchingCustomers: false,
        error: true,
        errormessage:action.payload
      }
    default:
      return state
  }
}