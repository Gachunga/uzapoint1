import  { POSTING_POS, POSTING_POS_SUCCESS, POSTING_POS_FAILURE } from '../actions/constants'
const initialState = {
  detail: [],
  isPosting: false,
  error: false,
  success: false,
  errormessage:''
}

export default function posapi (state = initialState, action) {
  switch (action.type) {
    case POSTING_POS:
      return {
        ...state,
        detail: [],
        isPosting: true
      }
    case POSTING_POS_SUCCESS:
      return {
        ...state,
        isPosting: false,
        success: true,
        detail: action.payload
      }
    case POSTING_POS_FAILURE:
      return {
        ...state,
        isPosting: false,
        error: true,
        errormessage:action.payload
      }
    case 'RESTORE_POS_STATE':
      return {
        ...state,
        isPosting: false,
        error: false,
        errormessage:''
      }
    default:
      return state
  }
}