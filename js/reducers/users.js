import { FETCHING_USERS, FETCHING_USERS_SUCCESS, FETCHING_USERS_FAILURE} from '../actions/constants'
const initialState = {
  users: [],
  isFetching: false,
  error: false
}

export default function users (state = initialState, action) {
  switch (action.type) {
    case FETCHING_USERS:
      return {
        ...state,
        users: [],
        isFetching: true
      }
    case FETCHING_USERS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        users: action.data
      }
    case FETCHING_USERS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}