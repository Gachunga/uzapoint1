import { FETCHING_OUTLETS, FETCHING_OUTLETS_SUCCESS, FETCHING_OUTLETS_FAILURE} from '../actions/constants'
const initialState = {
  outlets: [],
  isFetching: false,
  error: false
}

export default function outlets (state = initialState, action) {
  switch (action.type) {
    case FETCHING_OUTLETS:
      return {
        ...state,
        outlets: [],
        isFetching: true
      }
    case FETCHING_OUTLETS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        outlets: action.data
      }
    case FETCHING_OUTLETS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}