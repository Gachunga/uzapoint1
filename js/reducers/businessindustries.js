import { FETCHING_INDUSTRIES, FETCHING_INDUSTRIES_SUCCESS, FETCHING_INDUSTRIES_FAILURE} from '../actions/constants'
const initialState = {
  businessindustries: [],
  isFetching: false,
  error: false
}

export default function businessindustries (state = initialState, action) {
  switch (action.type) {
    case FETCHING_INDUSTRIES:
      return {
        ...state,
        businessindustries: [],
        isFetching: true
      }
    case FETCHING_INDUSTRIES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        businessindustries: action.data
      }
    case FETCHING_INDUSTRIES_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}