import { FETCHING_CATEGORIES, FETCHING_CATEGORIES_SUCCESS, FETCHING_CATEGORIES_FAILURE} from '../actions/constants'
const initialState = {
  businesscategories: [],
  isFetching: false,
  error: false
}

export default function businesscategories (state = initialState, action) {
  switch (action.type) {
    case FETCHING_CATEGORIES:
      return {
        ...state,
        businesscategories: [],
        isFetching: true
      }
    case FETCHING_CATEGORIES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        businesscategories: action.data
      }
    case FETCHING_CATEGORIES_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}