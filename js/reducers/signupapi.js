import  { SIGNNINGUP, SIGNUP_SUCCESS, SIGNUP_FAILURE } from '../actions/constants'
const initialState = {
  detail: [],
  isSigningup: false,
  error: false,
  success: false,
  errormessage:''
}

export default function signupapi (state = initialState, action) {
  switch (action.type) {
    case SIGNNINGUP:
      return {
        ...state,
        detail: [],
        isSigningup: true
      }
    case SIGNUP_SUCCESS:
      return {
        ...state,
        isSigningup: false,
        success: true,
        detail: action.payload
      }
    case SIGNUP_FAILURE:
      return {
        ...state,
        isSigningup: false,
        error: true,
        errormessage:action.payload
      }
    case 'RESTORE_STATE':
      return {
        ...state,
        isSigningup: false,
        error: false,
        errormessage:''
      }
    default:
      return state
  }
}