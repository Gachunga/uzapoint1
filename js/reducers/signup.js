import { ADD_BUSINESS_CATEGORIES, ADD_BUSINESS_INDUSTRIES, ADD_NUMBER_OF_OUTLETS, ADD_NUMBER_OF_USERS, ADD_BUSINESS_DETAILS, ADD_PERSONAL_DETAILS } from "../actions/constants";

const initialState = {
	completed: false,
    businessIndustry: {},
    businessCategories: {data:{}, completed:false},
    noOfOutlets: '',
    noOfUsers: '',
    businessDetails: {data:{}, completed:false},
    personalDetails: {data:{}, completed:false},
};
export default (state = initialState, { type, payload } ) => {
	switch(type){
		case 'ADD_BUSINESS_INDUSTRIES':
			return {
				...state,
				businessIndustry: payload,
			};
		case 'ADD_BUSINESS_CATEGORIES':
			return {
				...state,
				businessCategories:payload
            };
        case 'ADD_NUMBER_OF_OUTLETS':
			return {
				...state,
				noOfOutlets:payload
            };
        case 'ADD_NUMBER_OF_USERS':
			return {
				...state,
				noOfUsers:payload
            };
        case 'ADD_BUSINESS_DETAILS':
			return {
				...state,
				businessDetails:payload
            };
        case 'ADD_PERSONAL_DETAILS':
			return {
				...state,
				personalDetails:payload,
				completed:true,
			};
		case 'SIGNNINGUPUSER':
			return {
				...state,
				completed:false,
			};
		
		default:
			return state;
	}
};
