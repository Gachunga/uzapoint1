import  { AUTH_LOGGED, AUTH_LOGIN, LOGGING_IN, LOGIN_FAILURE, LOGIN_SUCCESS } from '../actions/constants'
const initialState = {
  user: [],
  isLogging: false,
  error: false,
  errormessage:'',
  successmsg: '',
  isActivating: false,
  activationError: false,
  activationErrorMessage:''
}

export default function login (state = initialState, action) {
  switch (action.type) {
    case LOGGING_IN:
      return {
        ...state,
        user: [],
        isLogging: true
      }
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLogging: false,
        user: action.data
      }
    case LOGIN_FAILURE:
      return {
        ...state,
        isLogging: false,
        error: true,
        errormessage:action.payload
      }
    case 'ACTIVATING':
      return {
        ...state,
        isActivating: true
      }
    case 'ACTIVATION_SUCCESS':
      return {
        ...state,
        isActivating: false,
        successmsg: action.payload
      }
    case 'ACTIVATION_FAILURE':
      return {
        ...state,
        isActivating: false,
        activationError: true,
        activationErrorMessage:action.payload
      }
    default:
      return state
  }
}