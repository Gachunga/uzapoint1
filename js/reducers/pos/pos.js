import  {ADD_CART_ITEM, ADD_CART_ITEMS, CLEAR_CART, REMOVE_ITEM, ADD_CUSTOMER, ADD_PAYMENT_DETAILS, SET_PRINT_STATUS, ADD_REFERENCE_NUMBER } from "../../actions/constants";

const initialState = {
	completed: false,
	category: null,
    cartItems: [],
    customerdetails: {},
	paymentdetails: [],
	ref_no: ''
};
export default (state = initialState, { type, payload } ) => {
	switch(type){
		case ADD_CART_ITEM:
			return {
				...state,
				cartItems: payload,
			};
		case ADD_CART_ITEMS:
			return {
				...state,
				cartItems:payload
            };
        case CLEAR_CART:
			return {
				...state,
				cartItems:[]
            };
        case REMOVE_ITEM:
			return {
				...state,
				cartItems:payload
            };
        case ADD_PAYMENT_DETAILS:
			return {
				...state,
				paymentdetails:payload
            };
        case ADD_CUSTOMER:
			return {
				...state,
				customerdetails:payload,
			};
		case ADD_REFERENCE_NUMBER:
			return {
				...state,
				ref_no:payload,
			};
		default:
			return state;
	}
};
