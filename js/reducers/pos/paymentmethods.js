import {FETCHING_PAYMENT_METHODS, FETCHING_PAYMENT_METHODS_SUCCESS, FETCHING_PAYMENT_METHODS_FAILURE } from '../../actions/constants'
const initialState = {
  paymentmethods: [],
  isFetchingPaymentMethods: false,
  error: false,
  errormsg: '',
  success: false,
}

export default function paymentmethods (state = initialState, action) {
  switch (action.type) {
    case FETCHING_PAYMENT_METHODS:
      return {
        ...state,
        paymentmethods: [],
        isFetchingPaymentMethods: true
      }
    case FETCHING_PAYMENT_METHODS_SUCCESS:
      return {
        ...state,
        isFetchingPaymentMethods: false,
        success: true,
        paymentmethods: action.payload
      }
    case FETCHING_PAYMENT_METHODS_FAILURE:
      return {
        ...state,
        isFetchingPaymentMethods: false,
        error: true,
        success:false,
        errormsg:action.payload
      }
    default:
      return state
  }
}