import { FETCHING_PRODUCT_LIST_UNCATEGORIZED, FETCHING_PRODUCT_LIST_UNCATEGORIZED_SUCCESS, FETCHING_PRODUCT_LIST_UNCATEGORIZED_FAILURE } from '../../actions/constants'
const initialState = {
  productlist: [],
  isFetchingProducts: false,
  error: false,
  errormsg: '',
  success: false,
}

export default function productsuncategorized (state = initialState, action) {
  console.log(action.type)
  switch (action.type) {
    case FETCHING_PRODUCT_LIST_UNCATEGORIZED:
      return {
        ...state,
        productlist: [],
        isFetchingProducts: true
      }
    case FETCHING_PRODUCT_LIST_UNCATEGORIZED_SUCCESS:
      return {
        ...state,
        isFetchingProducts: false,
        success: true,
        productlist: action.payload
      }
    case FETCHING_PRODUCT_LIST_UNCATEGORIZED_FAILURE:
      return {
        ...state,
        isFetchingProducts: false,
        error: true,
        success:false,
        errormsg:action.payload
      }
    default:
      return state
  }
}