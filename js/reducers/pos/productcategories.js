import { FETCHING_PRODUCT_CATEGORIES, FETCHING_PRODUCT_CATEGORIES_SUCCESS, FETCHING_PRODUCT_CATEGORIES_FAILURE } from '../../actions/constants'
const initialState = {
  categories: [],
  isFetchingCategories: false,
  error: false,
  errormsg: '',
  success: false,
}

export default function poscategories (state = initialState, action) {
  switch (action.type) {
    case FETCHING_PRODUCT_CATEGORIES:
      return {
        ...state,
        categories: [],
        isFetchingCategories: true
      }
    case FETCHING_PRODUCT_CATEGORIES_SUCCESS:
      return {
        ...state,
        isFetchingCategories: false,
        success: true,
        categories: action.payload
      }
    case FETCHING_PRODUCT_CATEGORIES_FAILURE:
      return {
        ...state,
        isFetchingCategories: false,
        error: true,
        success:false,
        errormsg:action.payload
      }
    default:
      return state
  }
}