import React, { Component } from "react";
import { TouchableHighlight, View, Text, StyleSheet } from 'react-native'

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchPeopleFromAPI } from '../actions/fetchpeople'

let styles

class App extends Component {

  

 render() {
  const {
    container,
    text,
    button,
    buttonText
  } = styles  
  const { people, isFetching } = this.props.people;
  return (
    <View style={container}>
      <Text style={text}>Redux Example</Text>
      <TouchableHighlight style={button} onPress={() => this.props.getPeople()}>
        <Text style={buttonText}>Load People</Text>
      </TouchableHighlight>
      {
        isFetching && <Spinner color="blue" />
      }
      {
        people.length ? (
          people.map((person, i) => {
            return <View key={i} >
              <Text>Name: {person.name}</Text>
              <Text>Birth Year: {person.birth_year}</Text>
            </View>
          })
        ) : null
      }
    </View>
  )
}
}

styles = StyleSheet.create({
  container: {
    marginTop: 100,
    paddingLeft: 20,
    paddingRight: 20
  },
  text: {
    textAlign: 'center'
  },
  button: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0b7eff'
  },
  buttonText: {
    color: 'white'
  }
})

function mapStateToProps (state, ownProps) {
    console.log(state)
  return {
    people: state.people
  }
}
 
function mapDispatchToProps (dispatch) {
  return {
    getPeople: () => dispatch(fetchPeopleFromAPI())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App) 