import React, { Component } from 'react';
import {  Footer, FooterTab, Button, Icon, Text, Badge } from 'native-base';

import styles from "./styles";
import CategoryModal from "./CategoryModal";

export default class FooterHome extends Component {
  render() {
    return (
        <Footer>
          <FooterTab style={styles.header}>
            <Button vertical transparent>
              <Icon name="home" style={{color: '#434858'}}/>
              <Text style={{color: '#434858'}}>Home</Text>
            </Button>
            <CategoryModal/>
            <Button active badge transparent>
              <Badge ><Text>51</Text></Badge>
              <Icon active name="notifications" style={{color: '#434858'}}/>
              <Text style={{color: '#434858'}}>Alerts</Text>
            </Button>
            <Button active badge transparent>
              <Badge ><Text>3</Text></Badge>
              <Icon active name="megaphone" style={{color: '#434858'}}/>
              <Text style={{color: '#434858'}}>Ads</Text>
            </Button>
            <Button active badge transparent>
              <Badge ><Text>10</Text></Badge>
              <Icon active name="pin" style={{color: '#434858'}}/>
              <Text style={{color: '#434858'}}>Near</Text>
            </Button>
          </FooterTab>
        </Footer>
    );
  }
}
