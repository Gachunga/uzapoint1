const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  headerContainer: {
    marginTop: 0,
    height: deviceHeight/4,
    backgroundColor : "#2befbe"
  },
  headerTextMain: {
    fontSize: 40,
    color : "#434858",
    marginTop: 40,
    alignSelf: 'center'
  },
  header: {
    backgroundColor : "#2befbe",
    // color: '#f5f6fa'
  },
  headerText: {
    alignSelf: 'center',
    color: '#434858'
  },
  ListItem: {
    borderColor: 'transparent',
    height : deviceHeight,
    alignSelf: 'center',
  },
  card: {
    backgroundColor: 'transparent',
    borderColor: '#2befbe',
    borderWidth: 2,

  },
  cardItem: {
    backgroundColor: 'transparent',
  },
  cardItemCategory: {
    backgroundColor: '#434858',
  },
  cardText: {
    color: '#2befbe',
    alignSelf: 'center'
  },
  contentContainer: {

  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    backgroundColor: "lightblue",
    padding: 12,
    margin: 16,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  modalContent: {
    backgroundColor: "white",
    padding: 8,
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)"
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0
  }
};
