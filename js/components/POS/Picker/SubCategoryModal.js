import React, { Component } from "react";
import {  TouchableOpacity, View, ScrollView } from "react-native";
import Modal from "react-native-modal";
import { Button, Icon, Text, Badge } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";
import CardItemCategory from "./CardItemCategory";

export default class SubCategoryModal extends Component {
  state = {
    visibleModal: null
  };
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  constructor(props) {
   super(props);
   this.state = {
     modalVisible: false,
   };
 }
  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>

    </View>
  );

  render() {
    return (
      <View>

        <Modal
              style={styles.bottomModal}
              onBackdropPress={() => {this.setModalVisible(false);}}
              isVisible={this.state.modalVisible}
        >
        <View style={styles.modalContent}>
          <ScrollView style={styles.contentContainer}>
            <Grid>
                <Row>
                  <Col>
                      <CardItemCategory/>
                   </Col>
                   <Col>
                       <CardItemCategory/>
                    </Col>
                    <Col>
                        <CardItemCategory/>
                     </Col>
                </Row>
                <Row>
                  <Col>
                      <CardItemCategory/>
                   </Col>
                   <Col>
                       <CardItemCategory/>
                    </Col>
                    <Col>
                        <CardItemCategory/>
                     </Col>
                </Row>
            </Grid>
          </ScrollView>
        </View>

        </Modal>
      </View>
    );
  }
}
