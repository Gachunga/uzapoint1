import React, { Component } from "react";
import { Image } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";

import { Spinner } from "native-base";

import { connect } from 'react-redux'

import { dispatchAction, actionsCreator } from "../../../actions/pos";

const imgbaseurl='http://heavenlyfood.uzahost.com/'

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class CardItemCol extends Component {
  constructor(props){
  super(props)
  }
  _addCartItems(){
    var items=this.props.pos.cartItems
    var item=this.props.data
    if(items.length==0){
    item.cart_quantity=1;
    items=[this.props.data]
    }else{
      let exists=0
      for(var i=0; i<items.length; i++){
        if(items[i].product_code==item.product_code){
          let qty=parseInt(item.cart_quantity)
          items[i].cart_quantity=parseInt(qty)+1
          exists=1
        }
      }
      if(!exists){
      item.cart_quantity=1
      items.push(item)
    }
    }
    console.log(items)
    this.props.createAction('addCartItems', items)
  }
  render() {
    let pic = {
      uri: imgbaseurl+this.props.data.image
    };
    return (
                      <Card style={styles.card}>
                       <CardItem style={styles.cardItem} button onPress={() => this._addCartItems()}>
                         <Body style={{backgroundColor: 'transparent'}}>
                           <Image source={pic}
                             style={{height: 100, width: 100, flex: 1, alignSelf: 'center'}}/> 
                           <Text style={styles.cardText}>
                             {this.props.data.product_name}
                           </Text>
                           <Text style={styles.cardText}>
                             {this.props.data.price}
                           </Text>
                           <Text style={styles.cardText}>
                             {this.props.data.uom_name}
                           </Text>
                         </Body>
                       </CardItem>
                     </Card>
    );
  }
}
function mapStateToProps (state, ownProps) {
return {
  pos:state.pos
}
}

function mapDispatchToProps (dispatch) {
return {
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(CardItemCol) 
