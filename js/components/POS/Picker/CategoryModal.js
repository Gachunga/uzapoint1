import React, { Component } from "react";
import {  TouchableOpacity, View, ScrollView, AsyncStorage } from "react-native";
import Modal from "react-native-modal";
import { Button, Icon, Text, Badge } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";
import CardItemCategory from "./CardItemCategory";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchproductcategories } from '../.././../actions/posapis'

export class CategoryModal extends Component {
  state = {
    visibleModal: null
  };
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  constructor(props) {
   super(props);
   this.state = {
     modalVisible: false,
   };
 }
  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.button}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalContent = () => (
    <View style={styles.modalContent}>

    </View>
  );

  componentDidMount(){
    this._sendData()
  }
  _sendData = async (code) => {
    try{
      let usera=await AsyncStorage.getItem('userdata');
      let userdata=JSON.parse(usera)
      let payload = {info: userdata}
      console.log(payload)
      this.props.fetchproductcategories(payload);
    }
    catch (error){
      console.log(error)
    }
  }
  render() {
    const { success, categories, isFetchingCategories } = this.props.poscategories
    const categorynames=Object.keys(categories)
    return (
      <View>
        <Button vertical badge transparent
        onPress={() => {
                  this.setModalVisible(true);
                }}>
          <Badge ><Text>5</Text></Badge>
          <Icon name="logo-buffer" style={{color: '#434858'}}/>
          <Text style={{color: '#434858'}}>Group</Text>
        </Button>
        <Modal
              style={styles.bottomModal}
              onBackdropPress={() => {this.setModalVisible(false);}}
              isVisible={this.state.modalVisible}
        >
        <View style={styles.modalContent}>
        {
                isFetchingCategories && <Spinner color="green" />
              }
          <ScrollView style={styles.contentContainer}
            >
            <Grid>
              {
            categorynames.length ? (
              categorynames.map((category, i) => {
                let newrow=true
                console.log(categorynames[i])
                let closerow=false
                let iseven= (i%2 == 0);
                let column2hasdata=typeof categorynames[i+1]
                if(iseven&& column2hasdata!='undefined'){
                  return (
                <Row key={(i+10).toString()}>
                  <Col>
                      <CardItemCategory key={i.toString()} data={category}/>
                  </Col>
                  
              
                <Col>
                    <CardItemCategory key={i.toString()} data={categorynames[i+1]}/>
                </Col>
                
              </Row>
                  )
                }else if(iseven &&column2hasdata=='undefined'){
                  return (
                    <Row key={(i+10).toString()}>
                      <Col>
                          <CardItemCategory key={i.toString()} data={category}/>
                      </Col>
                      
                  
                    <Col>
                        
                    </Col>
                    
                  </Row>
                      )
                }
                
              })
            ) : null
          }
            </Grid>
          </ScrollView>
        </View>

        </Modal>
      </View>
    );
  }
}
function mapStateToProps (state, ownProps) {
  console.info(state)
return {
  poscategories: state.poscategories
}
}

function mapDispatchToProps (dispatch) {
return {
  fetchproductcategories: (payload) => dispatch(fetchproductcategories(payload))
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(CategoryModal) 