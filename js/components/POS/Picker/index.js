import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView, Image, AsyncStorage } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem,
Header, Left, Right, Title, Badge, Drawer } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";
import CardItemCol from "./card";
import FooterHome from "./Footer";
import FabButton from "./FabButton";
import SideBarCart from '../SideBarCart';

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchuncategorizedproductlist } from '../.././../actions/posapis'


const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");


class Picker extends Component {
  constructor(props) {
    super(props);
    };
  state = {
    cartnumber: 0,
    password:'1qazwsx',
    showToast: false 
  }

  closeDrawer = () => {
    this.drawer._root.close()
  };
  openDrawer = () => {
    this.drawer._root.open()
  };
componentDidMount(){
  this._sendData()
}
_sendData = async (code) => {
  try{
    let usera=await AsyncStorage.getItem('userdata');
    let userdata=JSON.parse(usera)
    let payload = {info: userdata}
    console.log(payload)
    this.props.fetchuncategorizedproductlist(payload);
  }
  catch (error){
    console.log(error)
  }
}
_navigatetocart = () => {
  console.log('attempt to navigate')
  this.props.navigation.navigate('Cart')
}
  render() {
    const { success, productlist, isFetchingProducts } = this.props.productsuncategorized
    const { cartItems } = this.props.pos
    const no_of_items= cartItems.length
    return (
      <Drawer
        side="right"
        panOpenMask={.25}
        panCloseMask={.25}
        type="overlay"
        ref={(ref) => { this.drawer = ref; }}
        content={<SideBarCart _navigatetocart={this._navigatetocart}/>}
        onClose={() => this.closeDrawer()}
         >
      <Container>
        <Header searchBar rounded style={styles.header}>
          <Left style={{width: 5}}>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon active name="menu" style={{color: '#434858'}}/>
            </Button>
          </Left>
          <Body>
         <Item style={{width: 200}}>
           <Icon name="ios-search" />
           <Input placeholder="Search by product" style={{width: 200}}/>
         </Item>
         </Body>
         <Right >
           <Button vertical badge transparent style={{padding: 0, margin: 0}} onPress={() => this.drawer._root.open()}>
             <Badge style={{marginTop: 0, padding: 0, marginLeft: 25, marginBottom: 0}}><Text>{no_of_items}</Text></Badge>
             <Icon name="cart" style={{color: '#434858',marginTop: 0, padding: 0}}/>
           </Button>
         </Right>
         </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          <Content>
            <StatusBar
                  backgroundColor="#434858"
                  hidden={false}
                />
              {
                isFetchingProducts && <Spinner color="orange" />
              }
            <ScrollView style={styles.contentContainer}>
            <Grid>
              {
            productlist.length ? (
              productlist.map((product, i) => {
                let newrow=true
                let closerow=false
                let iseven= (i%2 == 0);
                let column2hasdata=typeof productlist[i+1]
                if(iseven&& column2hasdata!='undefined'){
                  return (
                <Row key={(i+10).toString()}>
                  <Col>
                      <CardItemCol key={i.toString()} data={product}/>
                  </Col>
                  
              
                <Col>
                    <CardItemCol key={i.toString()} data={productlist[i+1]}/>
                </Col>
                
              </Row>
                  )
                }else if(iseven &&column2hasdata=='undefined'){
                  return (
                    <Row key={(i+10).toString()}>
                      <Col>
                          <CardItemCol key={i.toString()} data={product}/>
                      </Col>
                      
                  
                    <Col>
                        
                    </Col>
                    
                  </Row>
                      )
                }
                
              })
            ) : null
          }
            </Grid>
            </ScrollView>

        </Content>
          <FabButton/>
          <FooterHome/>
        </ImageBackground>
      </Container>
      </Drawer>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.info(state)
return {
  productsuncategorized: state.productsuncategorized,
  pos: state.pos
}
}

function mapDispatchToProps (dispatch) {
return {
  fetchuncategorizedproductlist: (payload) => dispatch(fetchuncategorizedproductlist(payload))
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(Picker) 
