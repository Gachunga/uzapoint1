import React, { Component } from "react";
import { Image } from "react-native";
import { Text,
  Body, Card, CardItem } from "native-base";

import styles from "./styles";

import { Spinner } from "native-base";

import { connect } from 'react-redux'

import { dispatchAction, actionsCreator } from "../../../actions/signup";

const imgbaseurl='http://test.uzahost.com/'

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class CardItemCategory extends Component {
  constructor(props){
  super(props)
  }
  render() {
    console.log(this.props)
    // s
    return (
                      <Card style={styles.card}>
                       <CardItem style={styles.cardItemCategory} button onPress={() => this.props.createAction('addPosProducts', this.props.data)}>
                         <Body style={{backgroundColor: 'transparent'}}>
                           {/* <Image source={pic}
                             style={{height: 100, width: 100, flex: 1, alignSelf: 'center'}}/> */}
                           <Text style={styles.cardText}>
                             {this.props.data}
                           </Text>
                        
                         </Body>
                       </CardItem>
                     </Card>
    );
  }
}


function mapDispatchToProps (dispatch) {
return {
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapDispatchToProps
)(CardItemCategory) 
