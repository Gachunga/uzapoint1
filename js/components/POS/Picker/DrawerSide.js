import React, { Component } from 'react';
import {
  Text
} from 'react-native';

import {Drawer, View, Button, Icon} from 'native-base';


import SideBarCart from '../SideBarCart';
import Picker from '../Picker';

export default class DrawerSide extends Component {s
  closeDrawer = () => {
    this.drawer._root.close()
  };
  openDrawer = () => {
    this.drawer._root.open()
  };
  render() {
    return (
              <Drawer
                side="right"
                panOpenMask={.25}
                panCloseMask={.25}
                type="overlay"
                ref={(ref) => { this.drawer = ref; }}
                content={<SideBarCart/>}
                onClose={() => this.closeDrawer()} >
              <Picker/>
              </Drawer>
    );
  }
}
