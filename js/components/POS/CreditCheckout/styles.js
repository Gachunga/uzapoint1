const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  headerContainer: {
    marginTop: 0,
    height: deviceHeight/4,
    backgroundColor : "#2befbe"
  },
  headerTextMain: {
    fontSize: 40,
    color : "#434858",
    marginTop: 40,
    alignSelf: 'center'
  },
  headerText: {
    fontSize: 18,
    alignSelf: 'center',
  },
  header: {
    backgroundColor : "#434858",
    // color: '#f5f6fa'
  },
  headerText: {
    alignSelf: 'center',
    color: '#434858'
  },
  CheckoutView: {
    height : deviceHeight-deviceHeight/8,
    marginLeft : deviceWidth/12,
    marginRight : deviceWidth/12,
    backgroundColor : "#f5f6fa",
  },
  buttonView : {
    marginTop : 40,
  },
  ListItem: {
    borderColor: '#2befbe',
    height : deviceHeight/6,
    alignSelf: 'center',
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  }
};
