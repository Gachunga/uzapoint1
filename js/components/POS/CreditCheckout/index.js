import React, { Component } from "react";
import { ImageBackground, View, StatusBar, TouchableHighlight, AsyncStorage, DatePickerAndroid } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Body , Content, List, ListItem, Toast, Thumbnail, Header, Left, Right, Badge } from "native-base";

import { Col, Row, Grid } from 'react-native-easy-grid';

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchpaymentmethods } from '../../../actions/posapis'

import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/pos";


const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");
const imgbaseurl='http://test.uzahost.com/'
class CreditCheckout extends Component {
  state = {
    duedate: new Date().toLocaleDateString()
  }
  componentDidMount(){
  // this._sendData()
  }
  _sendData = async (code) => {
    try{
      let usera=await AsyncStorage.getItem('userdata');
      let userdata=JSON.parse(usera)
      let payload = {info: userdata}
      console.log(payload)
      this.props.getPaymentMethods(payload);
    }
    catch (error){
      console.log(error)
    }
  }

  _datepicker = async () => {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open({
        // Use `new Date()` for current date.
        // May 25 2020. Month 0 is January.
        date: new Date()
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        // Selected year, month (0-11), day
        var date = new Date(year, month, day);
        var datestring= date.toLocaleDateString()
        this.setState({duedate: datestring})
        console.log(this.state.duedate)
      }
    } catch ({code, message}) {
      console.warn('Cannot open date picker', message);
    }
  }

  _onclicknext = () => {
    const { businessCategories } = this.props.signup; 
    console.log(businessCategories.data)
    if (JSON.stringify(businessCategories.data)=='{}') {
    Toast.show({
      text: "Please select business category",
      buttonText: "Okay",
      duration: 1000
    })
    return;
    }
    this.props.navigation.navigate('Outlets')
    
  }
  _onclickback = () => {
    this.props.navigation.navigate('PaymentMethods')
    
  }

  render() {
    const { paymentmethods, isFetchingPaymentMethods } = this.props.paymentmethods;
    const { paymentdetails } = this.props.pos
    return (
      <Container>
        
         <Header searchBar rounded style={styles.header}>
          <Left style={{width: 5}}>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon active name="menu" style={{color: '#f5f6fa'}}/>
            </Button>
          </Left>
          <Body>
          <Text style={{alignSelf:'center', color: '#fff'}}>Credit Checkout</Text>
         </Body>
         <Right >
           
         </Right>
         </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          
          {
            isFetchingPaymentMethods && <Spinner color="red" />
          }
          <Content>
          <StatusBar
                  backgroundColor="#2befbe"
                  hidden={false}
                />
          <View style={styles.CheckoutView}>
          <View  style={{ backgroundColor: '#434858'}}>
            <Text style={{alignSelf: 'center', marginTop: 60, color: '#f5f6fa'}}>Your Credit limit</Text>
            <Button bordered success style={{ margin:5, alignSelf: 'center'}}
              
            >
              <Text style={{color:"#434858", fontWeight: "bold", color: '#f5f6fa'}}>500</Text>
            </Button>
          </View>
          <View style={{ backgroundColor: '#434858'}}>
          <View style={{ backgroundColor: '#2befbe', marginTop: 80, height: 40,
    justifyContent: 'center',
    alignItems: 'center'}}>
            <Text style={{ color: '#434858', fontSize: 18}}>Due Date</Text>
          </View>
          </View>
          <View style={{marginTop: 20,  marginLeft: 20, marginRight: 20,justifyContent: 'center',
    alignItems: 'center'}}>
            <Text style={{ justifyContent: 'center'}}>Click on the calender icon to select a date</Text>
            <Button style={{alignSelf: 'center'}} transparent onPress={ this._datepicker}>
              <Icon active name="calendar" style={{fontSize:60, color: '#434858'}}/>
            </Button>
            <Text style={{ justifyContent: 'center'}}>{this.state.duedate}</Text>
          </View>
          <View style={styles.buttonView}>
          <Grid>
            <Col>
            <Button block rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontWeight: "bold"}}>CANCEL</Text>
            </Button>
            </Col>
            <Col>
            <Button block rounded style={{backgroundColor:"#fff", margin:5}}
              onPress={() => this._onclicknext()}
            >
              <Text style={{color:"#434858", fontWeight: "bold"}}>DONE</Text>
            </Button>
            </Col>
            </Grid>
          </View>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  paymentmethods: state.paymentmethods,
  pos: state.pos,
}
}

function mapDispatchToProps (dispatch) {
return {
  getPaymentMethods: (payload) => dispatch(fetchpaymentmethods(payload)),
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(CreditCheckout) 

