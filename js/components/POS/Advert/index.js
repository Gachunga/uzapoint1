import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView, Image } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
   Body , Content, List, ListItem, Thumbnail, Header, Left, Right, Title,
  Card, CardItem } from "native-base";

import styles from "./styles";
import AdvertItem from "./AdvertItem";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class Advert extends Component {
  render() {
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent>
              <Icon name='menu' style={{color: '#434858'}}/>
            </Button>
          </Left>
          <Body>
            <Title  style={styles.headerText}>Adverts</Title>
          </Body>
          <Right>
            
          </Right>
        </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>

          <Content>
            <StatusBar
                  backgroundColor="#434858"
                  hidden={false}
                />
            <ScrollView style={styles.ScrollView} horizontal>
            <Card>
            <CardItem cardBody>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            <Thumbnail source={launchscreenLogo} style={styles.Thumbnail}/>
            </CardItem>
            </Card>
            </ScrollView>
            <AdvertItem/>
            <AdvertItem/>
            <AdvertItem/>
            <AdvertItem/>
            <AdvertItem/>
        </Content>
        </ImageBackground>
      </Container>
    );
  }
}

export default Advert;
