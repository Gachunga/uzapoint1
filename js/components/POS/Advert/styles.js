const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  header: {
    backgroundColor : "#2befbe",
    // color: '#f5f6fa'
  },
  headerText: {
    alignSelf: 'center',
    color: '#434858'
  },
  headerContainer: {
    marginTop: 0,
    height: deviceHeight/4,
    backgroundColor : "#2befbe"
  },
  CheckoutButton: {
    marginBottom: 0,
    padding: 20,
    backgroundColor:"#2befbe"
  },
  ScrollView: {
    width:deviceWidth,
    height:deviceHeight/8
  },
  Thumbnail: {
    margin:3,
  },
  headerTextMain: {
    fontSize: 40,
    color : "#434858",
    marginTop: 40,
    alignSelf: 'center'
  },
  ListItem: {
    borderColor: '#2befbe',
    height : deviceHeight/6,
    alignSelf: 'center',
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  }
};
