import React, { Component } from "react";
import {  Image } from "react-native";
import {  Button, H3, Text, Item, Input, Icon,
   Body , Content, List, ListItem, Thumbnail, Left, Right,
   Card, CardItem, Grid, Row, Col,
      } from "native-base";

import styles from "./styles";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class AdvertItem extends Component {
  render() {
    return (
              <Grid>
                <Row>

              <Card>
            <CardItem>
              <Left>
                <Thumbnail source={launchscreenLogo} />
                <Body>
                  <Text>Ad</Text>
                  <Text note>Uzapoint</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={launchscreenLogo} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
            <Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>Comments 4</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card>


        </Row>
      </Grid>
    );
  }
}

export default AdvertItem;
