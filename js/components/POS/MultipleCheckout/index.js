import React, { Component } from "react";
import { ImageBackground, View, StatusBar, TouchableHighlight, AsyncStorage, ScrollView } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Body , Content, List, ListItem, Toast, Thumbnail, Header, Left, Right, Badge } from "native-base";

import { Col, Row, Grid } from 'react-native-easy-grid';

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchpaymentmethods } from '../../../actions/posapis'

import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/pos";


const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");
const imgbaseurl='http://test.uzahost.com/'
class MultipleCheckout extends Component {
  state = {
    ref_no: '',
  }
  componentDidMount(){
  // this._sendData()
  }
  _sendData = async (code) => {
    try{
      let usera=await AsyncStorage.getItem('userdata');
      let userdata=JSON.parse(usera)
      let payload = {info: userdata}
      console.log(payload)
      this.props.getPaymentMethods(payload);
    }
    catch (error){
      console.log(error)
    }
  }

  _onclicknext = () => {
    const { businessCategories } = this.props.signup; 
    console.log(businessCategories.data)
    if (JSON.stringify(businessCategories.data)=='{}') {
    Toast.show({
      text: "Please select business category",
      buttonText: "Okay",
      duration: 1000
    })
    return;
    }
    //this.props.navigation.navigate('Outlets')
    
  }
  _onclickback = () => {
    
    this.props.navigation.navigate('PaymentMethods')
    
  }

  render() {
    const { paymentmethods, isFetchingPaymentMethods } = this.props.paymentmethods;
    const { paymentdetails } = this.props.pos
    return (
      <Container>
        
         <Header searchBar rounded style={styles.header}>
          <Left style={{width: 5}}>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon active name="menu" style={{color: '#f5f6fa'}}/>
            </Button>
          </Left>
          <Body>
          <Text style={{alignSelf:'center', color: '#fff'}}>Multiple Checkout</Text>
         </Body>
         <Right >
           
         </Right>
         </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          
          {
            isFetchingPaymentMethods && <Spinner color="red" />
          }
          <Content>
          <StatusBar
                  backgroundColor="#2befbe"
                  hidden={false}
                />
          <View style={styles.CheckoutView}>
         
          <View  style={{marginTop: 80, marginLeft: 30}}>
            <Text>Walk in customer</Text>
            <Text>Change</Text>
          </View>
          <ScrollView style={styles.SCrollView}>
          <View style={{marginTop: 80, marginLeft: 30}}>
            <Text>Selected Payment Method</Text>
            <Text>Change</Text>
          </View>
          <View style={{marginTop: 80, marginLeft: 20, marginRight: 20}}>
            <Text>Enter transaction reference number</Text>
            <Item rounded style={{ borderColor : "#434858" }}>
            <Input style={{ color: "#434858" }}
              placeholder='Enter Reference number'
              placeholderTextColor='#434858'
              returnKeyType='next'
              onChangeText={text => this.updateUsername(text)}
              value={this.state.ref_no}/>
          </Item>
          </View>
          <View style={{marginTop: 80, marginLeft: 30}}>
            <Text>Selected Payment Method</Text>
            <Text>Change</Text>
          </View>
          <View style={{marginTop: 80, marginLeft: 20, marginRight: 20}}>
            <Text>Enter transaction reference number</Text>
            <Item rounded style={{ borderColor : "#434858" }}>
            <Input style={{ color: "#434858" }}
              placeholder='Enter Reference number'
              placeholderTextColor='#434858'
              returnKeyType='next'
              onChangeText={text => this.updateUsername(text)}
              value={this.state.ref_no}/>
          </Item>
          </View>
          </ScrollView>
          <View style={styles.buttonView}>
          <Grid>
            <Col>
            <Button block rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontWeight: "bold"}}>CANCEL</Text>
            </Button>
            </Col>
            <Col>
            <Button block rounded style={{backgroundColor:"#fff", margin:5}}
              onPress={() => this._onclicknext()}
            >
              <Text style={{color:"#434858", fontWeight: "bold"}}>DONE</Text>
            </Button>
            </Col>
            </Grid>
          </View>
          
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  paymentmethods: state.paymentmethods,
  pos: state.pos,
}
}

function mapDispatchToProps (dispatch) {
return {
  getPaymentMethods: (payload) => dispatch(fetchpaymentmethods(payload)),
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(MultipleCheckout) 

