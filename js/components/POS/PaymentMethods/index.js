import React, { Component } from "react";
import { ImageBackground, View, StatusBar, TouchableHighlight, AsyncStorage } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Body , Content, List, ListItem, Toast, Thumbnail } from "native-base";

import { Col, Row, Grid } from 'react-native-easy-grid';

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchpaymentmethods } from '../../../actions/posapis'

import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/pos";


const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");
const imgbaseurl='http://test.uzahost.com/'
class PaymentMethods extends Component {
  componentDidMount(){
    this._sendData()
  }
  _sendData = async (code) => {
    try{
      let usera=await AsyncStorage.getItem('userdata');
      let userdata=JSON.parse(usera)
      let payload = {info: userdata}
      this.props.getPaymentMethods(payload);
    }
    catch (error){
      console.log(error)
    }
  }

  _onclicknext = () => {
    const { paymentdetails } = this.props.pos; 
    console.log(paymentdetails)
    if (paymentdetails.length===0) {
    Toast.show({
      text: "Please select Payment Method",
      buttonText: "Okay",
      duration: 1000
    })
    return;
    }
    if(paymentdetails[0]=='Credit'&&paymentdetails.length===1)
    this.props.navigation.navigate('CreditCheckout')
    else if(paymentdetails.length>1)
    this.props.navigation.navigate('MultipleCheckout')
    else{
      this.props.navigation.navigate('Checkout')
    }
    
  }
  _onclickback = () => {
    
    this.props.navigation.navigate('Cart')
  }
  _addPaymentMethodToArray = (method) => {
    let methods=this.props.pos.paymentdetails
    itemindex=methods.indexOf(method)
    if(methods.length===0){
    methods=[method]
    }else{
    if(itemindex==-1){
    methods.push(method)
    }else{
      methods.splice(itemindex, 1);
    }
    }
    console.log(methods)
    this.props.createAction('addPaymentDetails',methods)
  }
  
  render() {
    const { paymentmethods, isFetchingPaymentMethods } = this.props.paymentmethods;
    const { paymentdetails } = this.props.pos
    const paymentmethodsarr=Object.keys(paymentmethods)
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          <View style={styles.headerContainer}>
      
              <Text style={styles.headerTextMain}>Payment Methods</Text>
              <View style={{marginTop: 10}}>
              <Text style={styles.headerText}>Kindly choose your payment method</Text>
              <Text style={styles.headerText}>{
                 paymentdetails.length ? (
                  paymentdetails.map((method, i) => {
                    if(i>0)
                    method=', '+method
                    return (
                      method
                      )
                  })
                ) : null
                } </Text>
              </View>

          </View>
          {
            isFetchingPaymentMethods && <Spinner color="red" />
          }
          <Content>
          <List>
              {
            paymentmethodsarr.length ? (
              paymentmethodsarr.map((paymentmethod, i) => {
                let imgurl=paymentmethods[paymentmethod]
                let pmethod=paymentmethod.toString()
                return (
                <ListItem key={i} onPress={() => this._addPaymentMethodToArray(paymentmethod)}>
                   <Thumbnail square size={120} source={{uri: imgurl}} /> 
                    <Body>
                      <Text style={{color: 'white'}}>{paymentmethod}</Text>
                    </Body>
                  </ListItem>
                  )
              })
            ) : null
          }
          </List>
          <View style={{ padding: 20, marginTop: 10}}>
          <Grid>
            <Col>
            <Button block large rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontSize: 20, fontWeight: "bold"}}>CANCEL</Text>
            </Button>
            </Col>
            <Col>
            <Button block large rounded style={{backgroundColor:"#2befbe", margin:5}}
              onPress={() => this._onclicknext()}
            >
              <Text style={{color:"#434858", fontSize: 20, fontWeight: "bold"}}>DONE</Text>
            </Button>
            </Col>
            </Grid>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  paymentmethods: state.paymentmethods,
  pos: state.pos,
}
}

function mapDispatchToProps (dispatch) {
return {
  getPaymentMethods: (payload) => dispatch(fetchpaymentmethods(payload)),
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(PaymentMethods) 

