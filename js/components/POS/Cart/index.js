import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
   Body , Content, List, ListItem, Thumbnail, Header, Left, Right, Title } from "native-base";

import styles from "./styles";
import CartItem from "./CartItem";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchuncategorizedproductlist } from '../.././../actions/posapis'

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class Cart extends Component {
  render() {
    const { cartItems } = this.props.pos
    const no_of_items= cartItems.length
    let total=0
    return (
      <Container>
        <Header style={styles.header}>
          <Left>
            <Button transparent>
              <Icon name='menu' style={{color: '#434858'}}/>
            </Button>
          </Left>
          <Body>
            <Title  style={styles.headerText}>Home</Title>
          </Body>
          <Right>
            <Button transparent
              onPress={() => this.props.navigation.navigate("Login")}>
            <Icon ios="ios-trash" android="md-trash" style={{color: '#434858'}}/>
          </Button>
          </Right>
        </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>

          <Content>
            <StatusBar
                  backgroundColor="#434858"
                  hidden={false}
                />
          <ScrollView>
          {
          cartItems.length ? (
              cartItems.map((item, i) => {
               total=total+parseFloat(item.price*item.cart_quantity)
               item.cost=parseFloat(item.price*item.cart_quantity)
               //console.log(total)
                return (
                  <CartItem key={i} data={item}/>
                  )
                })
              )
             : null
            }
          </ScrollView>

        </Content>
        <View style={styles.CheckoutButton}>
          <Text style={{color:"#434858", fontSize: 20, fontWeight: "bold", alignSelf: 'center'}}>Total : {total}</Text>
          <Button block large rounded style={{backgroundColor:"#434858"}}
            onPress={() => this.props.navigation.navigate("PaymentMethods")}
          >
            <Text style={{color:"#2befbe", fontSize: 20, fontWeight: "bold"}}>Checkout</Text>
          </Button>
        </View>
        </ImageBackground>
      </Container>
    );
  }
}


function mapStateToProps (state, ownProps) {
  console.info(state)
return {
  productsuncategorized: state.productsuncategorized,
  pos: state.pos
}
}

function mapDispatchToProps (dispatch) {
return {
  fetchuncategorizedproductlist: (payload) => dispatch(fetchuncategorizedproductlist(payload))
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(Cart)
