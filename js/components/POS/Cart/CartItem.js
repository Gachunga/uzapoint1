import React, { Component } from "react";
import {  View } from "react-native";
import {  Button, H3, Text, Item, Input, Icon,
   Body , Content, List, ListItem, Thumbnail, Left, Right,
      } from "native-base";

import styles from "./styles";
const imgbaseurl='http://heavenlyfood.uzahost.com/'

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class CartItem extends Component {
  render() {
    let pic = {
      uri: imgbaseurl+this.props.data.image
    };
    return (

            <ListItem style={styles.ListItem}>
              <Thumbnail square size={80} source={pic} />
              <Body>
                <Text style={{color: '#2befbe'}}>{this.props.data.product_name}</Text>
                <Text style={{color: '#2befbe'}}note>Price: {this.props.data.currency+' '+this.props.data.price}</Text>
              </Body>
              <Button transparent
                >
              <Icon ios="ios-add" android="md-add" style={{color: '#f5f6fa'}}/>
              </Button>
              <Button style={{backgroundColor: '#2befbe'}}>
              <Text style={{marginTop: 0, paddingTop: 0, color: '#434858', fontSize: 20}}>{this.props.data.cart_quantity}</Text>
              </Button>
              <Button transparent
                >
              <Icon ios="ios-remove" android="md-remove" style={{color: '#f5f6fa'}}/>
              </Button>
            </ListItem>

    );
  }
}

export default CartItem;
