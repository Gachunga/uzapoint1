import React, { Component } from "react";
import { Image, View, ScrollView } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Grid,
  Col,Row,
  Badge,
  Button
} from "native-base";
import styles from "./styles";

import CartItem from "./CartItem";

import { Spinner } from "native-base";

import { connect } from 'react-redux'

const drawerCover = require("../../../../img/drawer-cover.png");
const drawerImage = require("../../../../img/logo.svg");


class SideBarCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
  }

  navigate = () => {
    this.props._navigatetocart()
  }
  
  render() {
    console.log(this.props._navigatetocart)
    const { cartItems } = this.props.pos
    const no_of_items= cartItems.length
    let total=0
    return (
      <Container style={styles.container}>
        <Content
          bounces={false}
          style={{ backgroundColor: "#fff" }}
        >
          <View style={styles.drawerCover} >
            <Grid>
            <Col>
            <Badge success style={{ alignSelf: 'center'}}><Text>Items {no_of_items}</Text></Badge>
            </Col>
            <Col>
            <Text style={{color:"#fff", fontSize: 20, fontWeight: "bold", alignSelf: 'center'}}>My Cart</Text>
            </Col>
          </Grid>
          </View>

          <ScrollView style={styles.ScrollView}>
          {
          cartItems.length ? (
              cartItems.map((item, i) => {
               total=total+parseFloat(item.price*item.cart_quantity)
               item.cost=parseFloat(item.price*item.cart_quantity)
               //console.log(total)
                return (
                  <CartItem key={i} data={item}/>
                  )
                })
              )
             : null
          }
          </ScrollView>

          <View style={styles.CheckoutButton}>
            <Text style={{color:"#434858", fontSize: 30, fontWeight: "bold", alignSelf: 'center'}}>Total : {total}</Text>
            <Button block large rounded style={{backgroundColor:"#434858", marginLeft: 20, marginRight: 20}}
             onPress={this.navigate}
            >
              <Text style={{color:"#2befbe", fontSize: 20, fontWeight: "bold"}}>Checkout</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
function mapStateToProps (state, ownProps) {
 // console.info(state)
return {
  pos: state.pos
}
}

function mapDispatchToProps (dispatch) {
return {
  fetchuncategorizedproductlist: (payload) => dispatch(fetchuncategorizedproductlist(payload))
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(SideBarCart)
