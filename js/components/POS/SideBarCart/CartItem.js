import React, { Component } from "react";
import {  View } from "react-native";
import {  Button, H3, Text, Item, Input, Icon,
   Body , Content, List, ListItem, Thumbnail, Left, Right,
      } from "native-base";

import styles from "./styles";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

const imgbaseurl='http://heavenlyfood.uzahost.com/'

class CartItem extends Component {
  constructor(props){
    super(props)
    }
    
  render() {
    let pic = {
      uri: imgbaseurl+this.props.data.image
    };
    return (

            <ListItem style={styles.ListItem}>
                <Left style={{width: 20}}>
                <Icon name="close"
                style={{height: 40,  color: 'red'}}/>
              </Left>
              <Body >
                <Text style={{width: 200, alignSelf: 'center'}}>{this.props.data.product_name}</Text>
                <Text note style={{width: 200, alignSelf: 'center'}}>Price: {this.props.data.currency+' '+this.props.data.price}</Text>
                <Text note style={{width: 200, alignSelf: 'center'}}>Quantity: {this.props.data.cart_quantity}</Text>
                <Text note style={{width: 200, alignSelf: 'center'}}>Cost: {this.props.data.cost}</Text>
              </Body>
              <Right>
              <Thumbnail square size={80} source={pic} />
              </Right>
            </ListItem>

    );
  }
}

export default CartItem;
