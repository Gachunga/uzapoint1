import React, { Component } from "react";
import { ImageBackground, View, StatusBar, TouchableHighlight, AsyncStorage, Alert } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Body , Content, List, ListItem, Toast, Thumbnail, Header, Left, Right, Badge } from "native-base";

import { Col, Row, Grid } from 'react-native-easy-grid';

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { postPosToAPI } from '../../../actions/postpos'

import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/pos";


const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");
const imgbaseurl='http://test.uzahost.com/'
class Checkout extends Component {
  state = {
    ref_no: '',
  }
  componentDidMount(){
  // this._sendData()
  }
  _sendData = async (code) => {
    try{
      let usera=await AsyncStorage.getItem('userdata');
      let userdata=JSON.parse(usera)
      let payload = {info: userdata, data: this.props.pos}
      console.log(payload)
      this.props.postPos(payload);
    }
    catch (error){
      console.log(error)
    }
  }

  _onclicknext = () => {
    const { customerdetails } = this.props.pos; 
    console.log(customerdetails)
    if (JSON.stringify(customerdetails)=='{}') {
    Toast.show({
      text: "Please select a customer",
      buttonText: "Okay",
      duration: 1000
    })
    return;
    }
    this._sendData()
    
  }
  _onclickback = () => {
    
    this.props.navigation.navigate('PaymentMethods')
    
  }

  _addRefNo = (text) => {
    this.setState({ref_no : text})
    this.props.createAction('addRefNo', text)
  }
  componentDidUpdate(){
  
    if(this.props.posapi.success){
      this.props.navigation.navigate('Picker')
    }
    if(this.props.posapi.error){
      let msg=this.props.posapi.errormessage
      if(msg.status='VALIDATE'){
        let err=''
        const message=msg.message
        const length=msg.length
        console.log(message)
        console.log(length)
        for(i=0;i<length;i++){
        console.log(legth)
        }
      
      Alert.alert(
        'Error',
        JSON.stringify(message),
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
      }
    }
    //logged && this.props.navigation.navigate('Picker')
  }
  render() {
    const { paymentmethods, isFetchingPaymentMethods } = this.props.paymentmethods;
    const { paymentdetails } = this.props.pos
    const { customerdetails } = this.props.pos
    let imgurl=paymentmethods[paymentdetails[0]]
    return (
      <Container>
        
         <Header searchBar rounded style={styles.header}>
          <Left style={{width: 5}}>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon active name="menu" style={{color: '#f5f6fa'}}/>
            </Button>
          </Left>
          <Body>
          <Text style={{alignSelf:'center', color: '#fff'}}>Checkout</Text>
         </Body>
         <Right >
           
         </Right>
         </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          
          {
            isFetchingPaymentMethods && <Spinner color="red" />
          }
          <Content>
          <StatusBar
                  backgroundColor="#2befbe"
                  hidden={false}
                />
          <View style={styles.CheckoutView}>
          <View  style={{marginTop: 80, marginLeft: 30}}>
            <Text>{customerdetails.name}</Text>
            <Button transparent 
            style={{margin:0, padding:0}}
            onPress={() => this.props.navigation.navigate("PosCustomers")}>
            <Text style={{color: '#434858'}}>Select Customer</Text>
            </Button>
          </View>
          <View style={{marginTop: 80, marginLeft: 30}}>
            <Text>Selected Payment Method: {<Thumbnail square size={80} source={{uri: imgurl}} /> }</Text>
            <Button transparent 
            style={{margin:0, padding:0}}
            onPress={() => this.props.navigation.goBack()}>
            <Text style={{color: '#434858'}}>Change</Text>
            </Button>
          </View>
          <View style={{marginTop: 80, marginLeft: 20, marginRight: 20}}>
            <Text>Enter transaction reference number</Text>
            <Item rounded style={{ borderColor : "#434858" }}>
            <Input style={{ color: "#434858" }}
              placeholder='Enter Reference number'
              placeholderTextColor='#434858'
              returnKeyType='next'
              onChangeText={text => this._addRefNo(text)}
              value={this.state.ref_no}/>
          </Item>
          </View>
          <View style={styles.buttonView}>
          <Grid>
            <Col>
            <Button block rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontWeight: "bold"}}>CANCEL</Text>
            </Button>
            </Col>
            <Col>
            <Button block rounded style={{backgroundColor:"#fff", margin:5}}
              onPress={() => this._onclicknext()}
            >
              <Text style={{color:"#434858", fontWeight: "bold"}}>DONE</Text>
            </Button>
            </Col>
            </Grid>
          </View>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  paymentmethods: state.paymentmethods,
  pos: state.pos,
  posapi: state.posapi,
}
}

function mapDispatchToProps (dispatch) {
return {
  postPos: (payload) => dispatch(postPosToAPI(payload)),
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(Checkout) 

