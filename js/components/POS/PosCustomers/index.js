import React, { Component } from "react";
import { ImageBackground, View, StatusBar, TouchableHighlight, FlatList, AsyncStorage } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Body , Content, List, ListItem, Toast, Header, Right, Left } from "native-base";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchCustomersFromAPI } from '../../../actions/fetchcustomers'

import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/pos";


const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");
const imgbaseurl='http://test.uzahost.com/'
class PosCustomers extends Component {
  componentDidMount(){
    this._sendData()
  }
  _sendData = async (code) => {
    try{
      let usera=await AsyncStorage.getItem('userdata');
      let userdata=JSON.parse(usera)
      let payload = {info: userdata}
      console.log(payload)
      this.props.getCustomers(payload);
    }
    catch (error){
      console.log(error)
    }
  }

  _selectCustomer = (customer) => {
    this.props.createAction('addCustomerDetail', customer)
    const { paymentdetails } = this.props.pos; 
    if (paymentdetails.length===0) {
      this.props.navigation.navigate('PaymentMethods')
    return;
    }
    if(paymentdetails[0]=='Credit'&&paymentdetails.length===1)
    this.props.navigation.navigate('CreditCheckout')
    else if(paymentdetails.length>1)
    this.props.navigation.navigate('MultipleCheckout')
    else{
      this.props.navigation.navigate('Checkout')
    }
    
  }

  render() {
    const { customers, isFetchingCustomers } = this.props.customers;
    console.log(customers)
    return (
      <Container>
        <Header searchBar rounded style={styles.header}>
          <Left style={{width: 5}}>
            <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")}>
              <Icon active name="menu" style={{color: '#f5f6fa'}}/>
            </Button>
          </Left>
          <Body>
          <Text style={{alignSelf:'center', color: '#fff'}}>Select Customer</Text>
         </Body>
         <Right >
           
         </Right>
         </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
         
          {
            isFetchingCustomers && <Spinner color="orange" />
          }
          <Content>
          <StatusBar
                  backgroundColor="#2befbe"
                  hidden={false}
                />
          <List>
          {
            customers.length ? (
              customers.map((customer, i) => {
                return (
                <ListItem key={i} onPress={() => this._selectCustomer(customer)}>
                    <Body>
                      <Text style={{color: 'white'}}>{customer.name}</Text>
                      <Text note style={{color: 'white'}}>{customer.email}</Text>
                    </Body>
                  </ListItem>
                  )
              })
            ) : null
          }
          </List>
        
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  customers: state.customers,
  pos: state.pos,
}
}

function mapDispatchToProps (dispatch) {
return {
  getCustomers: (payload) => dispatch(fetchCustomersFromAPI(payload)),
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(PosCustomers) 

