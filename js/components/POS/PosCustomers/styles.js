const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  headerContainer: {
    marginTop: 0,
    height: deviceHeight/4,
    backgroundColor : "#2befbe"
  },
  headerTextMain: {
    fontSize: 40,
    color : "#434858",
    marginTop: 40,
    alignSelf: 'center'
  },
  header: {
    backgroundColor : "#434858",
    // color: '#f5f6fa'
  },
  headerText: {
    alignSelf: 'center',
    color: '#f5f6fa'
  },
  ListItem: {
    borderColor: '#2befbe',
    height : deviceHeight/6,
    alignSelf: 'center',
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  }
};
