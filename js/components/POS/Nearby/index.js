import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
   Body , Content, List, ListItem, Thumbnail, Header, Left, Right, Title, Badge } from "native-base";

import styles from "./styles";
import NearbyItem from "./NearbyItem";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class Nearby extends Component {
  render() {
    return (
      <Container>
        <Header searchBar rounded style={styles.header}>
          <Left>
            <Button transparent style={{width: 35, margin: 0, padding: 0}}>
              <Icon active name="menu" style={{color: '#434858'}}/>
            </Button>
          </Left>
          <Body>
         <Item style={{width: 400}}>
           <Icon name="ios-search" />
           <Input placeholder="Search" style={{width: 300}}/>
         </Item>
         </Body>
         <Right />
         </Header>
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>

          <Content>
            <StatusBar
                  backgroundColor="#434858"
                  hidden={false}
                />
          <ScrollView>
            <NearbyItem/>
            <NearbyItem/>
            <NearbyItem/>
            <NearbyItem/>
            <NearbyItem/>
            <NearbyItem/>
          </ScrollView>

        </Content>
        </ImageBackground>
      </Container>
    );
  }
}

export default Nearby;
