import React, { Component } from "react";
import {  View } from "react-native";
import {  Button, H3, Text, Item, Input, Icon,
   Body , Content, List, ListItem, Thumbnail, Left, Right,
      } from "native-base";

import styles from "./styles";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class NearbyItem extends Component {
  render() {
    return (
      <ListItem style={styles.ListItem}>
          <Left style={{
    flex: 0,
    paddingLeft: 6,
    width: 62
}}>
          <Icon name="radio-button-on"
          style={{color: 'green'}}/>
        </Left>
        <Body style={{flex: 1, alignItems: 'flex-end'}}>
          <Text  style={{color : '#fff', fontSize: 20}}>White  Rogers Mututho</Text>
        </Body>
        <Right>
        <Thumbnail square size={80} source={launchscreenLogo} />
        </Right>
      </ListItem>
    );
  }
}

export default NearbyItem;
