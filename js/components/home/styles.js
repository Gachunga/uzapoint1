const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
  },
  logo: {
    flex: 1,
    position: "relative",
    left: Platform.OS === "android" ? 0 : 0,
    top: Platform.OS === "android" ? 40 : 0,
    alignSelf:'center',
    width: 320,
    height: 100
  },
  text: {
    color: "#434858",
    bottom: 6,
    marginTop: 5,
    fontSize: 20
  }
};
