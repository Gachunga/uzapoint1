import React, { Component } from "react";
import { ImageBackground, View, StatusBar, Content, Image,
  TouchableHighlight } from "react-native";
import { Container, Button, H3, Text } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
import ImageSlider from 'react-native-image-slider';

import styles from "./styles";

const slider3 = require("../../../img/slide2.png");
const slider2 = require("../../../img/slide3.png");
const slider1 = require("../../../img/splash2.png");
const launchscreenLogo = require("../../../img/logob.png");

class Home extends Component {
  render() {
    const images = [
     slider1,
     slider2,
     slider3
   ];

    return (
      <Container>
      <StatusBar hidden={true} />
        <ImageSlider
         images={images} style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>
          <View
            style={{
              alignItems: "center",
              marginBottom: 50,
              backgroundColor: "transparent",
            }}
          >
            <H3 style={styles.text}>Welcome to</H3>
            <View style={{ marginTop: 8 }} />
            <H3 style={styles.text}>Uzapoint</H3>
            <View style={{ marginTop: 8 }} />
          </View>


        </ImageSlider>
        <View style={{ marginBottom: 60 }}>
          <Grid>
            <Col style={{ backgroundColor: 'transparent', height: 100}}>
              <Button block large style={{backgroundColor:"#f5f6fa"}}
                onPress={() => this.props.navigation.navigate("BusinessIndustry")}
              >
                <Text style={{color:"#434858"}}>SIGN UP</Text>
              </Button>
                </Col>
            <Col style={{ backgroundColor: 'transparent', height: 100 }}>
            <Button block large style={{backgroundColor:"#434858"}}
                onPress={() => this.props.navigation.navigate("Login")}
              >
                <Text  style={{color:"#f5f6fa"}}>LOGIN</Text>
              </Button></Col>
          </Grid>
        </View>
      </Container>
    );
  }
}

export default Home;
