import React, { Component } from "react";
import { ImageBackground, View, StatusBar, TouchableHighlight } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Body , Content, List, ListItem, Toast, Thumbnail } from "native-base";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchBusinessIndustriesFromAPI } from '../../../actions/fetchbusinessindustries'

import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/signup";


const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");
const imgbaseurl='http://test.uzahost.com/'
class BusinessIndustry extends Component {
  componentDidMount(){
    this.props.getBusinessIndustries()

  }

  _onclicknext = () => {
    const { businessIndustry } = this.props.signup;
    console.log(JSON.stringify(businessIndustry)=='{}')
    if (JSON.stringify(businessIndustry)=='{}') {
    Toast.show({
      text: "Please select business industry",
      buttonText: "Okay",
      duration: 1000
    })
    return;
    }
    this.props.navigation.navigate('BusinessCategory')
    
  }

  render() {
    const { businessindustries, isFetching } = this.props.businessindustries;
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          <View style={styles.headerContainer}>
      
              <Text style={styles.headerTextMain}>Welcome!</Text>
              <View style={{marginTop: 10}}>
              <Text style={styles.headerText}>Kindly choose your Industry</Text>
              </View>

          </View>
          {
            isFetching && <Spinner color="orange" />
          }
          <Content>
          <List>
              {
            businessindustries.length ? (
              businessindustries.map((industry, i) => {
                let imgurl=imgbaseurl+industry.industry_image
                return (
                <ListItem key={i} onPress={() => this.props.createAction('addBusinessIndustries', industry)}>
                  <Thumbnail square size={80} source={{uri: imgurl}} />
                    <Body>
                      <Text style={{color: 'white'}}>{industry.industry_name}</Text>
                      <Text note style={{color: 'white'}}>{industry.industry_code}</Text>
                    </Body>
                  </ListItem>
                  )
              })
            ) : null
          }
          </List>
          <View style={{ padding: 20, marginTop: 40}}>
            <Button block large rounded style={{backgroundColor:"#2befbe"}}
              onPress={() => this._onclicknext()}
            >
              <Text style={{color:"#434858", fontSize: 20, fontWeight: "bold"}}>Next</Text>
            </Button>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  businessindustries: state.businessindustries,
  signup: state.signup,
}
}

function mapDispatchToProps (dispatch) {
return {
  getBusinessIndustries: () => dispatch(fetchBusinessIndustriesFromAPI()),
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(BusinessIndustry) 

