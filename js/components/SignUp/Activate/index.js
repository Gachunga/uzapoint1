import React, { Component } from "react";
import { ImageBackground, View, StatusBar, AsyncStorage, Alert } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Toast, Content } from "native-base";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { activateFromAPI } from '../.././../actions/login'

import styles from "./styles";

const launchscreenBg = require("../../../../img/login-processes_3.png");
const launchscreenLogo = require("../../../../img/logob.png");


class Activate extends Component {
  state = {
    code: 'BZ6071', 
  }
  
  activate = () => {
    if (this.state.code === '') return;
    let code= this.state.code
    this._sendData(code)
  }
  resend = () => {
    if (this.state.inputValue === '') return;
   
  }
  componentDidUpdate(){
    const { activationError, activationErrorMessage, successmsg } = this.props.login;
    console.log(this.props.login)
    if(activationError){
      Toast.show({
        text: activationErrorMessage,
        buttonText: "Okay",
        duration: 3000
      })
      return
    }
    if(successmsg!=''){
      Alert.alert(
        'Success',
        successmsg,
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: true }
      )
      this.props.navigation.navigate('Picker')
    }
  }
 
  _sendData = async (code) => {
    try{
      let usera=await AsyncStorage.getItem('userdata');
      let userdata=JSON.parse(usera)
      let payload = {code: code, info: userdata}
      console.log(payload)
      this.props.activate(payload);
    }
    catch (error){
      console.log(error)
    }
  }
  render() {
    const { isActivating } = this.props.login;
    console.log(this.props)
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>

          <View
            style={{
              alignItems: "center",
              marginBottom: 20,
              marginLeft: 25,
              marginRight:25,
              backgroundColor: "transparent",
            }}
          >
          {
                isActivating && <Spinner color="blue" />
          }
           
          <Item style={{ borderColor : "#434858" }}>
            <Input style={{ color: "#434858" , alignSelf:'center',textAlign: 'center'}}
              placeholder='Enter Code'
              placeholderTextColor='#434858'
              returnKeyType='next'
              onChangeText={text => this.setState({code:text})}
              value={this.state.code}/>
          </Item>
          </View>

          <View style={{ 
            marginTop : 10, alignItems: "center",
            marginBottom: 60,
            marginLeft: 20,
            marginRight:20,
            backgroundColor: "transparent"
             }}>
            <Text style={{justifyContent:"center"}}> We've sent an activation code to your email</Text>
            <Text style={{justifyContent:"center"}}> Please check your inbox</Text>
            <Button bordered rounded dark style={{ alignSelf:'center', marginTop: 10}}
             
              onPress={() => this.resend()}
            >
              <Text>RESEND</Text>
            </Button>
            </View>
         
          <View style={{
              marginBottom: 50,
              marginLeft: 10,
              marginRight:10,
            }}>
            <Button block rounded style={{backgroundColor:"#434858"}}
             
              onPress={() => this.activate()}
            >
              <Text>SIGN UP</Text>
            </Button>    
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.info(state)
return {
  login: state.login
}
}

function mapDispatchToProps (dispatch) {
return {
  activate: (payload) => dispatch(activateFromAPI(payload))
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(Activate) 
