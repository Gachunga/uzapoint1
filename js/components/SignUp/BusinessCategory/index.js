import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView, Image } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem, Toast } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";
import CardItemCol from "./card";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchBusinessCategoriesFromAPI } from '../../../actions/fetchbusinesscategories'

import { dispatchAction, actionsCreator } from "../../../actions/signup";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");
class BusinessCategory extends Component {
  componentDidMount(){
    this.props.getBusinessCategories()

  }

  _onclicknext = () => {
    const { businessCategories } = this.props.signup; 
    console.log(businessCategories.data)
    if (JSON.stringify(businessCategories.data)=='{}') {
    Toast.show({
      text: "Please select business category",
      buttonText: "Okay",
      duration: 1000
    })
    return;
    }
    this.props.navigation.navigate('Outlets')
    
  }
  _onclickback = () => {
    
    this.props.navigation.navigate('BusinessIndustry')
    
  }
  render() {
    const { businesscategories, isFetching } = this.props.businesscategories;
    let datalength=businesscategories.length
    console.log(datalength)
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          <View style={styles.headerContainer}>

              <Text style={styles.headerTextMain}>Lets get started!</Text>
              <View style={{marginTop: 10}}>
              <Text style={styles.headerText}>Kindly choose your Business Category</Text>
              </View>

          </View>
          <Content>
            <ScrollView style={styles.contentContainer}>
            {
            isFetching && <Spinner color="orange" />
          }
          <Grid>
              {
            businesscategories.length ? (
              businesscategories.map((category, i) => {
                let newrow=true
                console.log(businesscategories[i])
                let closerow=false
                let iseven= (i%2 == 0);
                let column2hasdata=typeof businesscategories[i+1]
                if(iseven&& column2hasdata!='undefined'){
                  return (
                <Row key={(i+10).toString()}>
                  <Col>
                      <CardItemCol key={i.toString()} data={category}/>
                  </Col>
                  
              
                <Col>
                    <CardItemCol key={i.toString()} data={businesscategories[i+1]}/>
                </Col>
                
              </Row>
                  )
                }else if(iseven &&column2hasdata=='undefined'){
                  return (
                    <Row key={(i+10).toString()}>
                      <Col>
                          <CardItemCol key={i.toString()} data={category}/>
                      </Col>
                      
                  
                    <Col>
                        
                    </Col>
                    
                  </Row>
                      )
                }
                
              })
            ) : null
          }
            </Grid>
            </ScrollView>
          <View style={{ padding: 20, marginTop: 40}}>
          <Grid>
            <Col>
            <Button block large rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontSize: 20, fontWeight: "bold"}}>Back</Text>
            </Button>
            </Col>
            <Col>
            <Button block large rounded style={{backgroundColor:"#2befbe", margin:5}}
              onPress={() => this._onclicknext()}
            >
              <Text style={{color:"#434858", fontSize: 20, fontWeight: "bold"}}>Next</Text>
            </Button>
            </Col>
            </Grid>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  businesscategories: state.businesscategories,
  signup: state.signup,
}
}

function mapDispatchToProps (dispatch) {
return {
  getBusinessCategories: () => dispatch(fetchBusinessCategoriesFromAPI())
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(BusinessCategory) 
