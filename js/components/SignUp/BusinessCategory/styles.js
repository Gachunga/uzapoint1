const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  headerContainer: {
    marginTop: 0,
    height: deviceHeight/4,
    backgroundColor : "#2befbe"
  },
  headerTextMain: {
    fontSize: 40,
    color : "#434858",
    marginTop: 40,
    alignSelf: 'center'
  },
  headerText: {
    fontSize: 18,
    alignSelf: 'center',
  },
  ListItem: {
    borderColor: 'transparent',
    height : deviceHeight/6,
    alignSelf: 'center',
  },
  card: {
    backgroundColor: 'transparent',
    borderColor: '#2befbe',
    borderWidth: 2,

  },
  cardItem: {
    backgroundColor: 'transparent',
  },
  cardText: {
    color: '#2befbe',
    alignSelf: 'center'
  },
  contentContainer: {
    height : deviceHeight/2,
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  }
};
