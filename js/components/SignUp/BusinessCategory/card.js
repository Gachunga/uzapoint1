import React, { Component } from "react";
import { Image } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";

import { Spinner } from "native-base";

import { connect } from 'react-redux'

import { dispatchAction, actionsCreator } from "../../../actions/signup";

const imgbaseurl='http://test.uzahost.com/'

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class CardItemCol extends Component {
  constructor(props){
  super(props)
  }
  render() {
    console.log(this.props)
    let pic = {
      uri: imgbaseurl+this.props.data.image_url
    };
    return (
                      <Card style={styles.card}>
                       <CardItem style={styles.cardItem} button onPress={() => this.props.createAction('addBusinessCategories', this.props.data)}>
                         <Body style={{backgroundColor: 'transparent'}}>
                           <Image source={pic}
                             style={{height: 100, width: 100, flex: 1, alignSelf: 'center'}}/>
                           <Text style={styles.cardText}>
                             {this.props.data.name}
                           </Text>
                         </Body>
                       </CardItem>
                     </Card>
    );
  }
}
function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  users: state.users,
  signup: state.signup,
}
}

function mapDispatchToProps (dispatch) {
return {
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(CardItemCol) 
