import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView, Image } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem, Toast } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchBusinessIndustriesFromAPI } from '../../../actions/fetchbusinessindustries'

import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/signup";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class BusinessDetails extends Component {
  state = {
    name: 'uzapointdev',
    email:'johnkachunga@gmail.com',
    phone:'0706387057',
    location:'kiambu',
    country:'kenya',
    postal_address:'10204',
    showToast: false 
  }
  
  addBusinessDetails = () => {
    if (this.state.name === ''||
        this.state.email === ''||
        this.state.phone === ''||
        this.state.location === ''||
        this.state.country === ''||
        this.state.postal_address === ''){
        Toast.show({
          text: "Please fill all fields!",
          buttonText: "Okay",
          duration: 1000
        })
         return;
        }
        if(this.state.country='kenya'){
          this.state.country='KE'
        }
    payload={
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      location: this.state.location,
      country: this.state.country,
      postal_address: this.state.postal_address,
    };
    this.props.createAction('addBusinessDetails', payload)
    Toast.show({
      text: "Business Details Added!",
      buttonText: "Okay",
      duration: 500
    })
    this.props.navigation.navigate('PersonalDetails')
  }
  _onclickback = () => {
    
    this.props.navigation.navigate('NoOfUsers')
    
  }
  render() {
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          <View style={styles.headerContainer}>

              <Text style={styles.headerTextMain}>Almost there!</Text>
              <View style={{marginTop: 10}}>
              <Text style={styles.headerText}>Kindly provide the following business details</Text>
              </View>

          </View>
          <Content>
            <ScrollView style={styles.contentContainer}>
                <View style={{ marginTop: 10, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-briefcase" android="md-briefcase" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                    placeholder='Business Name'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({name:text})}
                      value={this.state.name}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-mail" android="md-mail" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Business Email'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({email:text})}
                      value={this.state.email}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-call" android="md-call" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Phone Number'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({phone:text})}
                      value={this.state.phone}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-pin" android="md-pin" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Location'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({location:text})}
                      value={this.state.location}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-map" android="md-map" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Country'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({country:text})}
                      value={this.state.country}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-send" android="md-send" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Postal Address'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({postal_address:text})}
                      value={this.state.postal_address}/>
                  </Item>
                </View>
            </ScrollView>
          <View style={{ padding: 20, marginTop: 40}}>
          <Grid>
            <Col>
            <Button block large rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontSize: 20, fontWeight: "bold"}}>Back</Text>
            </Button>
            </Col>
            <Col>
            <Button block large rounded style={{backgroundColor:"#2befbe", margin:5}}
              onPress={() => this.addBusinessDetails()}
            >
              <Text style={{color:"#434858", fontSize: 20, fontWeight: "bold"}}>Next</Text>
            </Button>
            </Col>
            </Grid>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  signup: state.signup,
}
}

function mapDispatchToProps (dispatch) {
return {
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(BusinessDetails) 
