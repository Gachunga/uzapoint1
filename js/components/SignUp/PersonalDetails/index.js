import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView, Image, Alert, 
AsyncStorage } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import styles from "./styles";
import { dispatchAction, actionsCreator } from "../../../actions/signup";

import { postSignUpToAPI } from '../../../actions/postsignup'

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class PersonalDetails extends Component {
  state = {
    name: 'uzapointdev',
    email:'jgachunga@clemcreativity.com',
    phone:'0706387057',
    last_name:'kiambu',
    password:'kenya1',
    confirm_password:'kenya1',
    showToast: false 
  }
  
  addPersonalDetails = () => {
    if (this.state.name === ''||
        this.state.email === ''||
        this.state.phone === ''||
        this.state.last_name === ''||
        this.state.password === ''||
        this.state.confirm_password === ''){
        Toast.show({
          text: "Please fill all fields!",
          buttonText: "Okay",
          duration: 1000
        })
         return;
        }
    payload={
      name: this.state.name,
      email: this.state.email,
      phone: this.state.phone,
      last_name: this.state.last_name,
      password: this.state.password,
      confirm_password: this.state.confirm_password,
    };
    this.props.createAction('addPersonalDetails', payload)

    
    //this.props.navigation.navigate('PersonalDetails')
  }
  
  _onclickback = () => {
    
    this.props.navigation.navigate('BusinessDetails')
    
  }
  componentDidUpdate(){
    let payload = this.props.signup
    if(this.props.signup.completed){
    this.props.signUpUser(
      payload
    );
    }
    if(this.props.signupapi.success){
      let userdata=this.props.signupapi.detail
      AsyncStorage.setItem('userdata', JSON.stringify(userdata))
      this.props.navigation.navigate('Activate')
    }
    if(this.props.signupapi.error){
      let msg=this.props.signupapi.errormessage
      if(msg.status='VALIDATE'){
        let err=''
        const message=msg.message
        const length=msg.length
        console.log(message)
        console.log(length)
        for(i=0;i<length;i++){
        console.log(legth)
        }
      
      Alert.alert(
        'Error',
        JSON.stringify(message),
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
      }
    }
    //const { error, errormessage } = this.props.login;

    // if(error){
    //   Toast.show({
    //     text: errormessage,
    //     buttonText: "Okay",
    //     duration: 3000
    //   })
    // }
    
    //logged && this.props.navigation.navigate('Picker')
  }
  render() {
    const { isSigningup } = this.props.signupapi;
    console.log(isSigningup)
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          <View style={styles.headerContainer}>

              <Text style={styles.headerTextMain}>Final Step!</Text>
              <View style={{marginTop: 10}}>
              <Text style={styles.headerText}>Kindly provide the following personal details</Text>
              </View>

          </View>
          {
                isSigningup && <Spinner color="blue" />
          }
          <Content>
            <ScrollView style={styles.contentContainer}>
                <View style={{ marginTop: 10, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-person" android="md-person" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='First Name'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({name:text})}
                      value={this.state.name}/>
                  </Item>
                </View>
                <View style={{ marginTop: 10, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-person-add" android="md-person-add" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Last Name'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({last_name:text})}
                      value={this.state.last_name}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-mail" android="md-mail" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Email'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({email:text})}
                      value={this.state.email}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-call" android="md-call" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Phone Number'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      onChangeText={text => this.setState({phone:text})}
                      value={this.state.phone}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-key" android="md-key" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Password'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      secureTextEntry={true}
                      onChangeText={text => this.setState({password:text})}
                      value={this.state.password}/>
                  </Item>
                </View>
                <View style={{ marginTop: 8, padding: 10}} >
                  <Item style={{ borderColor : '#2befbe'}}>
                    <Icon ios="ios-key" android="md-key" style={{ color: '#2befbe' }} />
                    <Input style={{ color: "#fff" }}
                      placeholder='Confirm Password'
                      placeholderTextColor='#2befbe'
                      returnKeyType='next'
                      secureTextEntry={true}
                      onChangeText={text => this.setState({confirm_password:text})}
                      value={this.state.password}/>
                  </Item>
                </View>
            </ScrollView>
          <View style={{ padding: 20, marginTop: 40}}>
           <Grid>
            <Col>
            <Button block large rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontSize: 20, fontWeight: "bold"}}>Back</Text>
            </Button>
            </Col>
            <Col>
            <Button block large rounded style={{backgroundColor:"#2befbe", margin:5}}
              onPress={() => this.addPersonalDetails()}
            >
              <Text style={{color:"#434858", fontSize: 20, fontWeight: "bold"}}>Sign Up</Text>
            </Button>
            </Col>
            </Grid>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}
function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  signupapi:state.signupapi,
  signup: state.signup
}
}

function mapDispatchToProps (dispatch) {
return {
  signUpUser: (payload) => dispatch(postSignUpToAPI(payload)),
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(PersonalDetails) 
