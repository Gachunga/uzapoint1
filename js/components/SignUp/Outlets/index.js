import React, { Component } from "react";
import { ImageBackground, View, StatusBar, ScrollView, Image } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";
import CardItemCol from "./card";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchOutletsFromAPI } from '../../../actions/fetchoutlets'

import { dispatchAction, actionsCreator } from "../../../actions/signup";

const launchscreenBg2 = require("../../../../img/login-processes_2.png");
const launchscreenLogo = require("../../../../img/Advantage_Plus.jpg");

class Outlets extends Component {
  componentDidMount(){
    this.props.getOutlets()

  }

  _onclicknext = () => {
    const { noOfOutlets } = this.props.signup; 
    console.log(noOfOutlets)
    if (JSON.stringify(noOfOutlets)=='{}') {
    Toast.show({
      text: "Please select number of outlets",
      buttonText: "Okay",
      duration: 1000
    })
    return;
    }
    this.props.navigation.navigate('NoOfUsers')
    
  }
  _onclickback = () => {
    
    this.props.navigation.navigate('BusinessCategory')
    
  }

  render() {
    const { outlets, isFetching } = this.props.outlets;
    let datalength=outlets.length
    console.log(datalength)
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg2} style={styles.imageContainer}>
          <View style={styles.headerContainer}>

              <Text style={styles.headerTextMain}>Tell us more!</Text>
              <View style={{marginTop: 10}}>
              <Text style={styles.headerText}>Kindly choose the number of outlets</Text>
              </View>

          </View>
          <Content>
          <ScrollView style={styles.contentContainer}>
            {
            isFetching && <Spinner color="orange" />
          }
          <Grid>
              {
            outlets.length ? (
              outlets.map((outlet, i) => {
                let newrow=true
                console.log(outlets[i])
                let closerow=false
                let iseven= (i%2 == 0);
                let column2hasdata=typeof outlets[i+1]
                if(iseven&& column2hasdata!='undefined'){
                  return (
                <Row key={(i+10).toString()}>
                  <Col>
                      <CardItemCol key={i.toString()} data={outlet}/>
                  </Col>
                  
              
                <Col>
                    <CardItemCol key={i.toString()} data={outlets[i+1]}/>
                </Col>
                
              </Row>
                  )
                }else if(iseven &&column2hasdata=='undefined'){
                  return (
                    <Row key={(i+10).toString()}>
                      <Col>
                          <CardItemCol key={i.toString()} data={outlet}/>
                      </Col>
                      
                  
                    <Col>
                        
                    </Col>
                    
                  </Row>
                      )
                }
                
              })
            ) : null
          }
            </Grid>
            </ScrollView>
          <View style={{ padding: 20, marginTop: 40}}>
          <Grid>
            <Col>
            <Button block large rounded style={{backgroundColor:"#434858", margin:5}}
              onPress={() => this._onclickback()}
            >
              <Text style={{color:"#2befbe", fontSize: 20, fontWeight: "bold"}}>Back</Text>
            </Button>
            </Col>
            <Col>
            <Button block large rounded style={{backgroundColor:"#2befbe", margin:5}}
              onPress={() => this._onclicknext()}
            >
              <Text style={{color:"#434858", fontSize: 20, fontWeight: "bold"}}>Next</Text>
            </Button>
            </Col>
            </Grid>
          </View>
        </Content>

        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  outlets: state.outlets,
  signup: state.signup,
}
}

function mapDispatchToProps (dispatch) {
return {
  getOutlets: () => dispatch(fetchOutletsFromAPI())
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(Outlets) 

