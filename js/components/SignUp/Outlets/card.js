import React, { Component } from "react";
import { Image } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon,
  Body , Content, List, ListItem, Thumbnail, Card, CardItem } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';

import styles from "./styles";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { fetchBusinessCategoriesFromAPI } from '../../../actions/fetchbusinesscategories'

import { dispatchAction, actionsCreator } from "../../../actions/signup";

class CardItemCol extends Component {
  constructor(props){
    super(props)
    }
    render() {
      let data=this.props.data
    return (
                      <Card style={styles.card}>
                       <CardItem style={styles.cardItem} button onPress={() => this.props.createAction('addNoOfOutlets', data)}>
                         <Body style={{backgroundColor: 'transparent'}}>
                           <Text style={styles.cardText}>
                            {data.noOfStores}
                           </Text>
                         </Body>
                       </CardItem>
                     </Card>
    );
  }
}


function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  outlets: state.outlets,
  signup: state.signup,
}
}

function mapDispatchToProps (dispatch) {
return {
  createAction: (action, payload) => dispatchAction(action, payload)
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(CardItemCol) 