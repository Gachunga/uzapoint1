import React, { Component } from "react";
import { ImageBackground, View, StatusBar, AsyncStorage } from "react-native";
import { Container, Button, H3, Text, Item, Input, Icon, Toast, Content } from "native-base";

import { Spinner } from "native-base";

import { connect } from 'react-redux'
import { loginFromAPI } from '../../actions/login'

import styles from "./styles";

const launchscreenBg = require("../../../img/login-processes_3.png");
const launchscreenLogo = require("../../../img/logob.png");


class Login extends Component {
  state = {
    username: 'support@uzapoint.com',
    password:'1qazwsx',
    showToast: false 
  }
  
  loginPerson = () => {
    if (this.state.inputValue === '') return;
    this.props.loginUser({
      username: this.state.username,
      password: this.state.password
    });
    //this.setState({ username: '', password:'' });
  }
  showToast = () => {
    Toast.show({
      text: "Wrong password!",
      buttonText: "Okay",
      duration: 3000
    })
    //this.setState({ inputValue: '' });
  }
  updateUsername = (inputValue) => {
    this.setState({
      username: inputValue
    });
  }
  updatePassword = (inputValue) => {
    this.setState({
      password: inputValue
    });
  }
  componentDidUpdate(){
    const { logged } = this.props.auth;
    const { error, errormessage } = this.props.login;

    if(error){
      Toast.show({
        text: errormessage,
        buttonText: "Okay",
        duration: 3000
      })
    }
    if(logged){
      let userdata=this.props.login.user
      AsyncStorage.setItem('userdata', JSON.stringify(userdata))
    }
    
    logged && this.props.navigation.navigate('Picker')
    
  }

  componentWillUnmount() {

  }

  render() {
    const { login, isLogging } = this.props.login;
    const { logged } =this.props.auth
    console.log(this.state)
    return (
      <Container>
        <StatusBar hidden={true} />
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>

          <View
            style={{
              alignItems: "center",
              marginBottom: 1,
              marginLeft: 10,
              marginRight:10,
              backgroundColor: "transparent",
            }}
          >
          {
                isLogging && <Spinner color="blue" />
          }
           
          <Item rounded style={{ borderColor : "#434858" }}>
            <Icon active name='person' style={{ color: "#434858" }} />
            <Input style={{ color: "#434858" }}
              placeholder='Username'
              placeholderTextColor='#434858'
              returnKeyType='next'
              onChangeText={text => this.updateUsername(text)}
              value={this.state.username}/>
          </Item>
            <View style={{ marginTop: 8 }} />
              <Item rounded style={{ borderColor : "#434858" }}>
                <Icon active name='key' style={{ color: "#434858" }}/>
                <Input 
                  style={{ color: "#434858" }}
                  placeholder='Password'
                  placeholderTextColor='#434858'
                  returnKeyType='go'
                  secureTextEntry={true}
                  onChangeText={text => this.updatePassword(text)}
                  value={this.state.password}
                />
              </Item>
            <View style={{ marginTop: 8 }} />
          </View>
          <View style={{
              marginBottom: 50,
              marginLeft: 10,
              marginRight:10,
            }}>
            <Button block rounded style={{backgroundColor:"#434858"}}
             
              onPress={this.loginPerson}
            >
              <Text>LOGIN</Text>
            </Button>    
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

function mapStateToProps (state, ownProps) {
  console.log(state)
return {
  login: state.login,
  auth: state.auth, 
  nav: state.nav
}
}

function mapDispatchToProps (dispatch) {
return {
  loginUser: (user) => dispatch(loginFromAPI(user))
}
}

export default connect(
mapStateToProps,
mapDispatchToProps
)(Login) 
