const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;

export default {
  imageContainer: {
    flex: 1,
    width: null,
    height: null
  },
  logoContainer: {
    flex: 1,
    marginTop: deviceHeight / 8,
  },
  logo: {
    position: "relative",
    left: Platform.OS === "android" ? 0 : 0,
    top: Platform.OS === "android" ? 40 : 0,
    alignSelf:'center',
    width: 320,
    height: 100
  },
  text: {
    color: "#D8D8D8",
    bottom: 6,
    marginTop: 5
  }
};
