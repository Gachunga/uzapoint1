import React, { Component } from "react";
import { Image, View } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge
} from "native-base";
import styles from "./style";

const drawerCover = require("../../../img/drawer-cover.png");
const drawerImage = require("../../../img/logo.svg");
const datas = [
  {
    name: "Login",
    route: "Login",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Home",
    route: "Home",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Sign Up",
    route: "SignUp",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Adverts",
    route: "Advert",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "POS",
    route: "Picker",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Cart",
    route: "Cart",
    icon: "phone-portrait",
    bg: "#C5F442"
  },
  {
    name: "Nearby",
    route: "Nearby",
    icon: "phone-portrait",
    bg: "#C5F442"
  }
];

class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
  }

  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
          <View style={styles.drawerCover} ></View>
          <Image square style={styles.drawerImage} source={drawerImage} />

          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.name}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Items`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>}
          />
        </Content>
      </Container>
    );
  }
}

export default SideBar;
