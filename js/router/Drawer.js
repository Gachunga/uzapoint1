import React from "react";
import { DrawerNavigator } from "react-navigation";

import Home from "../components/home/";
import Login from "../components/Login/";
import Modal from "../components/Modal/";
import BusinessIndustry from "../components/SignUp/BusinessIndustry";
import BusinessCategory from "../components/SignUp/BusinessCategory";
import Outlets from "../components/SignUp/Outlets";
import NoOfUsers from "../components/SignUp/NoOfUsers";
import BusinessDetails from "../components/SignUp/BusinessDetails";
import PersonalDetails from "../components/SignUp/PersonalDetails";
import Activate from "../components/SignUp/Activate";
import Picker from "../components/POS/Picker";
import Cart from "../components/POS/Cart";
import Nearby from "../components/POS/Nearby";
import Advert from "../components/POS/Advert";
import PaymentMethods from "../components/POS/PaymentMethods";
import Checkout from "../components/POS/Checkout";
import CreditCheckout from "../components/POS/CreditCheckout";
import MultipleCheckout from "../components/POS/MultipleCheckout";
import PosCustomers from "../components/POS/PosCustomers";
import DrawerSide from "../components/POS/Picker/DrawerSide";
import SideBarCart from "../components/POS/SideBarCart";
import SideBar from "../components/sidebar";
import People from "../components/people";

const Drawer = DrawerNavigator(
  {
    Home: { screen: Home },
    Login: { screen: Login },
    BusinessIndustry: { screen: BusinessIndustry },
    BusinessCategory: { screen: BusinessCategory },
    Outlets: { screen: Outlets },
    NoOfUsers: { screen: NoOfUsers },
    BusinessDetails: { screen: BusinessDetails },
    PersonalDetails: { screen: PersonalDetails },
    Picker: { screen: Picker },
    Cart: { screen: Cart },
    DrawerSide: { screen: DrawerSide },
    Nearby: { screen: Nearby },
    Advert: { screen: Advert },
    Activate: { screen: Activate },
    People: { screen: People },
    PaymentMethods: { screen: PaymentMethods},
    Checkout: { screen: Checkout},
    CreditCheckout: { screen: CreditCheckout},
    MultipleCheckout: { screen: MultipleCheckout},
    PosCustomers: { screen: PosCustomers},
  },  
  {
    initialRouteName: "Picker",
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    contentComponent: props => <SideBar  {...props} />
  }
);
export default Drawer;
